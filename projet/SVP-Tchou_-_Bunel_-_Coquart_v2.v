(**** Projet de SVP ****)
(**** Kevin Coquart ****)
(**** Quentin Bunel ****)



(** Machine TchouTchou**)

(* Require *)
Require Import Arith.
Require Import List.


(* Utilitaires *)
(* La liste l contient-elle l'élément e ? *)
(* oui -> true *)
(* non -> false *)
Fixpoint containsB (e:nat) (l:list nat) : bool :=
  match l with
      | h::q => (andb (beq_nat h e) (containsB e q))
      | nil => false
  end.

(* La liste l contient-elle l'élément e ? *)
(* oui -> True *)
(* non -> False *)
Fixpoint contains (e:nat) (l:list nat) : Prop :=
  match l with
      | h::q => h = e \/ contains e q
      | nil => False
  end.


(* La liste lCantons contient-elle tout les éléments secondaire de la liste lTrainsCantons ? *)
(* oui -> True *)
(* non -> False *)
Fixpoint containsAll (lTrainsCantons : list (nat*nat)) (lCantons : list nat) : Prop :=
  match lTrainsCantons with
      | (a,b)::q => (contains b lCantons) /\ (containsAll q lCantons)
      | nil => True
  end.

(* L'élément e appartient-il a la liste l ? *)
(* oui -> False *)
(* non -> True *)
Fixpoint notExist (e:nat) (l:list nat) : Prop :=
  match l with 
    | h::q => h <> e /\ notExist e q
    | nil => True
  end.

(* Chaque élément de la liste l est-il unique ? *)
(* oui -> False *)
(* non -> True *)
Fixpoint unique (l : list nat) : Prop :=
  match l with
    | h::q => (notExist h q) /\ (unique q)
    | nil => True
  end.


(* Extrait la sous liste des clés d'une liste d'association. *)
(* ((1,2) (3,4) (5,6)) => (1 3 5) *)
Fixpoint listNatNatToListNat1 (l: list (nat*nat)) : list nat :=
  match l with
    | (a, b)::q => (cons a (listNatNatToListNat1 q))
    | nil => nil
  end.

(* Extrait la sous liste des valeurs d'une liste d'association. *)
(* ((1,2) (3,4) (5,6)) => (2 4 6) *)
Fixpoint listNatNatToListNat2 (l: list (nat*nat)) : list nat :=
  match l with
    | (a, b)::q => (cons b (listNatNatToListNat2 q))
    | nil => nil
  end.

(* Extrait l'élément associé à la clé dans la liste d'association. *)
(* 3 && ((1,2) (3,4) (5,6)) => 4 *)
Fixpoint associateElement (elem1: nat) (list: list (nat*nat)) : nat :=
  match list with 
    | (a, b)::q => if (beq_nat elem1 a) then b else associateElement elem1 q
    | nil => 0
  end.



(** Machine TchouTchou **)
Module TchouTchou.





(* === Etat === *)
Record State : Set := 
  mkState {
      (* liste associative entre un capteur et son successeur, forme le circuit des trains. *)
      circuits: list (nat * nat);

      (* liste des cantons actifs (désignés par un numéro)  *)
      cantons: list nat;
      
      (* liste des numéros des trains qui roulent *)
      trains: list nat;
      
      (* emplacement des trains : liste d'association  num_train -> num_canton *)
      emplacements: list (nat*nat)
    }.




(* Invariants *)
(* Inv_1 : tous les cantons occupés sont au rouge (actif)
      -->  tous les nums de canton de emplacements sont aussi dans listeCantons  *)
Definition Inv_1 (B:State) : Prop :=
  containsAll B.(emplacements) B.(cantons).
  
(* Inv_2 : deux trains ne sont jamais au même emplacement.
 --> tous les nums cantons de emplacements sont différents *)
Definition Inv_2 (B:State) : Prop :=
  unique (listNatNatToListNat2 B.(emplacements)).

(* Inv_3 : un train qui roule se dirige tjs vers un emplacement libre *)
Definition Inv_3 (B:State) : Prop :=
  forall train : nat, 
    (contains train B.(trains)) ->
    notExist (associateElement (associateElement train B.(emplacements)) B.(circuits)) B.(cantons).














(*### Evenement : Init ###*)
Module Init.

(* == Action == *)
Definition action : State :=
  (* circuits | cantons | trains | emplacements *)
  mkState ((1,2)::(2,3)::(3,1)::nil) (1::2::nil) (20::nil) ((10,1)::(20,2)::nil).


(* Inv_1 : tous les cantons occupés sont au rouge (actif)
      -->  tous les nums de canton de emplacements sont aussi dans listeCantons  *)
Lemma PO_Safety_Inv_1: 
  let B := action in Inv_1 B.
Proof.
  intro B.	
  unfold Inv_1.
  simpl.
  auto.  
Qed.
  

(* Inv_2 : deux trains ne sont jamais au même emplacement.
 --> tous les nums cantons de emplacements sont différents *)
Lemma PO_Safety_Inv_2: 
 let B := action in Inv_2 B.
Proof.	
  intros B.
  unfold Inv_2.
  simpl.
  auto.
Qed.


(* Inv_3 : un train qui roule se dirige tjs vers un emplacement libre *)
Lemma PO_Safety_Inv_3: 
  let B := action in Inv_3 B.
Proof.	
  intros B.
  unfold Inv_3.
  intros train Hcontains.
  unfold action in B.
  simpl circuits.
  simpl trains in Hcontains.
  simpl contains in Hcontains.
  elim Hcontains.
  - (* train = 20 *)
    intro t20.
    rewrite <- t20.
    simpl.
    auto with arith.
  - (* False *)
    intro HFalse.
    inversion HFalse.
Qed.


(* == Globale == *)
Theorem PO_Safety:
 let B := action in Inv_1 B /\ Inv_2 B /\ Inv_3 B.
Proof.
  intro B.
  split.
  - apply PO_Safety_Inv_1 ; assumption.
  - split.
   + apply PO_Safety_Inv_2 ; assumption.
   + apply PO_Safety_Inv_3 ; assumption.
Qed.


End Init.
(*### END : Init ###*)












(*### Evenement : Stop ###*)
Module Stop.

(* == Action == *)
Definition action (B:State) : State :=
  (* circuits | cantons | trains | emplacements *)
  mkState B.(circuits) B.(cantons) nil B.(emplacements).

(* Inv_1 : tous les cantons occupés sont au rouge (actif)
      -->  tous les nums de canton de emplacements sont aussi dans listeCantons  *)
Lemma PO_Safety_Inv_1: 
  forall (B:State),
    (* Invariants *)
    Inv_1 B
    -> Inv_2 B 
    -> Inv_3 B
    (* ==> post *)
    -> let B' := action B  in Inv_1 B'.
Proof.	
  intros B H1 H2 H3 B'.
  unfold B'. clear B'.
  unfold action.
  unfold Inv_1 in *.
  simpl.
  exact H1.
Qed.
  
(* Inv_2 : deux trains ne sont jamais au même emplacement.
 --> tous les nums cantons de emplacements sont différents *)
Lemma PO_Safety_Inv_2: 
  forall (B:State),
    (* Invariants *)
    Inv_1 B
    -> Inv_2 B 
    -> Inv_3 B
    (* ==> post *)
    -> let B' := action B  in Inv_2 B'.
Proof.
  intros B H1 H2 H3 B'.
  unfold B'. clear B'.
  unfold action.
  unfold Inv_2 in *.
  simpl.
  exact H2.	
Qed.

(* Inv_3 : un train qui roule se dirige tjs vers un emplacement libre *)
Lemma PO_Safety_Inv_3: 
  forall (B:State),
    (* Invariants *)
    Inv_1 B
    -> Inv_2 B 
    -> Inv_3 B
    (* ==> post *)
    -> let B' := action B  in Inv_3 B'.
Proof.
  intros B H1 H2 H3 B'.
  unfold B'. clear B'.
  unfold action.
  unfold Inv_3 in *.
  simpl.
  tauto.	
Qed.

Theorem PO_Safety:
  forall (B:State),
    (* Invariants *)
    Inv_1 B
    -> Inv_2 B
    -> Inv_3 B
    (* ==> post *)  
    -> let B' := action B  in Inv_1 B' /\ Inv_2 B' /\ Inv_3 B'.
Proof.
  intro B.
  split.
  - apply PO_Safety_Inv_1 ; assumption.
  - split.
   + apply PO_Safety_Inv_2 ; assumption.
   + apply PO_Safety_Inv_3 ; assumption.
Qed.

End Stop.
(*### END : Stop ###*)











(*### Evenement : Avance ###*)
Module Avance.

(* calcul le futur état des cantons à partir de l'état actuel des trains et de leurs emplacements.
Si le train roule, alors il active le canton suivant. Sinon, il active son canton actuel. *)
Fixpoint nextStateCantons (circuits: list (nat * nat)) (trains : list nat) (emplacements : list (nat * nat)) : list nat :=
  match emplacements with
  | (a, b) :: q => if (containsB a trains)
                      then (associateElement b circuits) :: (nextStateCantons circuits trains q) 
                      else b :: (nextStateCantons circuits trains q)
  | nil => nil
  end.

(* calcul les futurs emplacements des trains à partir de l'état actuel des trains et de leur emplacements.
Le train roulait, il sera à l'emplacement suivant, sinon, il ne bouge pas. *)
Fixpoint nextStateEmplacements (circuits: list (nat * nat)) (trains : list nat) (emplacements : list (nat * nat)) : list (nat * nat) :=
  match emplacements with
  | (a, b) :: q => if (containsB a trains)
                     then (a, (associateElement b circuits)) :: (nextStateEmplacements circuits trains q)
                     else (a, b) :: (nextStateEmplacements circuits trains q)
  | nil => nil
  end.

(* calcul les trains qui vont rouler à partir des futurs emplacements et capteurs actifs.
Si le capteur suivant l'emplacement est inactif, il démarre, sinon, il ne bouge pas. *)
Fixpoint nextStateTrains (circuits: list (nat * nat)) (nextCantons : list nat) (nextEmplacements : list (nat * nat)) : list nat :=
  match nextEmplacements with
  | (a, b) :: q => if containsB (associateElement b circuits) nextCantons
                      then (nextStateTrains circuits nextCantons q)
                      else a :: (nextStateTrains circuits nextCantons q)
  | nil => nil
  end.



(* == Action == *)
Definition action (B:State) : State :=
  (* circuits | cantons | trains | emplacements *)
  let nextCantons := nextStateCantons B.(circuits) B.(trains) B.(emplacements) in
   let nextEmplacements := nextStateEmplacements B.(circuits) B.(trains) B.(emplacements) in
    let nextTrains := nextStateTrains B.(circuits) nextCantons nextEmplacements in 
     mkState B.(circuits) nextCantons nextTrains nextEmplacements.






(* Inv_1 : tous les cantons occupés sont au rouge (actif)
      -->  tous les nums de canton de emplacements sont aussi dans listeCantons  *)
Lemma PO_Safety_Inv_1: 
  forall (B:State),
    (* Invariants *)
    Inv_1 B
    -> Inv_2 B 
    -> Inv_3 B
    (* ==> post *)
    -> let B' := action B  in Inv_1 B'.
Proof.	
  intros B H1 H2 H3 B'.
  unfold B'. clear B'.
  unfold action.
  unfold Inv_1 in *.
  unfold Inv_2 in *.
  unfold Inv_3 in *.
  simpl in *.
  induction emplacements.
  - (* cas nil *)
   simpl.
   trivial.
  - (* a :: l *)
   admit. (* A COMPLETER *)
Qed.
  
(* Inv_2 : deux trains ne sont jamais au même emplacement.
 --> tous les nums cantons de emplacements sont différents *)
Lemma PO_Safety_Inv_2: 
  forall (B:State),
    (* Invariants *)
    Inv_1 B
    -> Inv_2 B 
    -> Inv_3 B
    (* ==> post *)
    -> let B' := action B  in Inv_2 B'.
Proof.
  intros B H1 H2 H3 B'.
  unfold B'. clear B'.
  unfold action.
  unfold Inv_2 in *.
  simpl.
  induction emplacements.
  - (* cas nil *)
   simpl.
   trivial.
  - (* a :: l *)
   simpl.
   admit. (* A COMPLETER *)
Qed.


(* Inv_3 : un train qui roule se dirige tjs vers un emplacement libre *)
Lemma PO_Safety_Inv_3: 
  forall (B:State),
    (* Invariants *)
    Inv_1 B
    -> Inv_2 B 
    -> Inv_3 B
    (* ==> post *)
    -> let B' := action B  in Inv_3 B'.
Proof.
  intros B H1 H2 H3 B'.
  unfold B'. clear B'.
  unfold action.
  unfold Inv_3 in *.
  simpl.
  induction emplacements.
  - (* cas nil *)
   simpl.
   tauto.
  - (* a :: l *)
   admit. (* A COMPLETER *)
Qed.


(* == Globale == *)
Theorem PO_Safety:
  forall (B:State),
    (* Invariants *)
    Inv_1 B
    -> Inv_2 B
    -> Inv_3 B
    -> let B' := action B  in Inv_1 B' /\ Inv_2 B' /\ Inv_3 B'.
Proof.
  intro B.
  split.
  - apply PO_Safety_Inv_1 ; assumption.
  - split.
    + apply PO_Safety_Inv_2 ; assumption.
    + apply PO_Safety_Inv_3 ; assumption.
Qed.


End Avance.
(*### END : Avance ###*)


End TchouTchou.
(* END :  Machine TchouTchou *)
