(**** Projet de SVP ****)
(**** Kevin Coquart ****)
(**** Quentin Bunel ****)




(** Machine Tchou **)
Module Tchou.
  
(* Définition inductive des emplacements, cela correspond à l'emplacement des capteurs du circuit. *)
Inductive emplacement : Set := C1 : emplacement | C2 : emplacement | C3 : emplacement.

(* Le circuit est un anneau simple, chaque capteur est alignée dans un ordre precis
 *)
Definition next(e:emplacement) : emplacement :=
  match e with
    | C1 => C2
    | C2 => C3
    | C3 => C1
  end.


(* Definit l'egalite entre deux emplacements. *)
Definition equalEmplacement(e1:emplacement) (e2:emplacement) : bool :=
  match (e1,e2) with
    | (C1,C1) => true
    | (C1,C2) => false
    | (C1,C3) => false
    | (C2,C2) => true
    | (C2,C1) => false
    | (C2,C3) => false
    | (C3,C3) => true
    | (C3,C1) => false
    | (C3,C2) => false
  end.

(* Test True *)
Example equalEmpTrue : equalEmplacement C1 C1 = true.
Proof.
  compute.
  reflexivity.
Qed.

(* Test False *)
Example equalEmpFalse : equalEmplacement C1 C2 = false.
Proof.
  compute.
  reflexivity.
Qed.




(* ~~ Contexte : constantes et axiomes ~~ *)
Module Context.
  Axiom dif12 : C1 <> C2.
  Axiom dif13 : C1 <> C3.
  Axiom dif23 : C2 <> C3.
End Context.




(* === Etat === *)
Record State : Set := 
  mkState {
      (* etat du capteur c1 / c2 / c3 *)
      actif_c1: bool; (* true = occupé = rouge *)
      actif_c2: bool;
      actif_c3: bool;
      
      (* etat des trains t1 / t2 *)
      roule_t1: bool; (* true = roule *)
      roule_t2: bool;
      
      (* emplacement des trains t1 / t2 *)
      emplacement_t1: emplacement;
      emplacement_t2: emplacement
    }.




(* === Invariants === *)
(* Inv_1 : il y a tjs un capteur inactif et deux actifs. *)
Definition Inv_1 (B:State) : Prop :=
  (B.(actif_c1) = true /\ B.(actif_c2) = true /\ B.(actif_c3) = false)
  \/ (B.(actif_c1) = false /\ B.(actif_c2) = true /\ B.(actif_c3) = true)
  \/ (B.(actif_c1) = true /\ B.(actif_c2) = false /\ B.(actif_c3) = true).

(* Inv_2 : les trains ne sont jamais au même emplacement. *)
Definition Inv_2 (B:State) : Prop :=
  (equalEmplacement B.(emplacement_t1) B.(emplacement_t2)) = false.

(* Inv_3a : le capteur 1 est actif ssi un train est à l'emplacement C1. *)
Definition Inv_3a (B:State) : Prop :=
  B.(actif_c1) = (if (orb (equalEmplacement C1 B.(emplacement_t1))
                          (equalEmplacement C1 B.(emplacement_t2))) then true else false). 

(* Inv_3b : le capteur 2 est actif ssi un train est à l'emplacement C2. *)
Definition Inv_3b (B:State) : Prop :=
  B.(actif_c2) = (if (orb (equalEmplacement C2 B.(emplacement_t1))
                          (equalEmplacement C2 B.(emplacement_t2))) then true else false).

(* Inv_3c : le capteur 3 est actif ssi un train est à l'emplacement C3. *)
Definition Inv_3c (B:State) : Prop :=
  B.(actif_c3) = (if (orb (equalEmplacement C3 B.(emplacement_t1))
                          (equalEmplacement C3 B.(emplacement_t2))) then true else false).







(*### Evenement : Init ###*)
Module Init.
  
(* == Action == *)
Definition action : State :=
  mkState true true false false false C1 C2.

(* Inv_1 : il y a tjs un capteur inactif et deux actifs. *)
Lemma PO_Safety_Inv_1: 
  let B := action in Inv_1 B.
Proof.
  intro B.	
  unfold Inv_1.
  simpl.
  left.
  split.
  reflexivity.
  split.
  reflexivity.
  reflexivity.
Qed.

(* Inv_2 : les trains ne sont jamais au même emplacement. *)
Lemma PO_Safety_Inv_2: 
  let B := action in Inv_2 B.
Proof.	
  intro B.
  unfold Inv_2.
  simpl.
  compute.
  reflexivity.
Qed.

(* Inv_3a : le capteur 1 est actif ssi un train est à l'emplacement C1. *)
Lemma PO_Safety_Inv_3a: 
  let B := action in Inv_3a B.
Proof.	
  intro B.
  unfold Inv_3a.
  simpl.
  reflexivity.
Qed.

(* Inv_3b : le capteur 2 est actif ssi un train est à l'emplacement C2. *)
Lemma PO_Safety_Inv_3b: 
  let B := action in Inv_3b B.
Proof.	
  intro B.
  unfold Inv_3a.
  simpl.
  reflexivity.
Qed.


(* Inv_3c : le capteur 3 est actif ssi un train est à l'emplacement C3. *)
Lemma PO_Safety_Inv_3c: 
  let B := action in Inv_3c B.
Proof.	
  intro B.
  unfold Inv_3c.
  simpl.
  reflexivity.
Qed.

(* == Globale == *)
Theorem PO_Safety:
  let B := action in Inv_1 B /\ Inv_2 B /\ Inv_3a B /\ Inv_3b B /\ Inv_3c B.
Proof.
  intro B.
  split.
  - apply PO_Safety_Inv_1 ; assumption.
  - split.
    + apply PO_Safety_Inv_2 ; assumption.
    + split.
      * apply PO_Safety_Inv_3a ; assumption.
      * split. 
        apply PO_Safety_Inv_3b ; assumption.
        apply PO_Safety_Inv_3c ; assumption.
Qed.
End Init.
(*### END : Init ###*)





(*### Evenement : Stop ###*)
Module Stop.

(* == Action == *)
Definition action (B:State) : State :=
  mkState B.(actif_c1) B.(actif_c2) B.(actif_c3) false false B.(emplacement_t1) B.(emplacement_t2).

(* Inv_1 : il y a tjs un capteur inactif et deux actifs. *)
Lemma PO_Safety_Inv_1: 
  forall (B:State),
    (* Invariants *)
    Inv_1 B
    -> Inv_2 B
    -> let B' := action B  in Inv_1 B'.
Proof.	
  intros B H1 H2 B'.
  unfold B'. clear B'.
  unfold action.
  unfold Inv_1 in *.
  simpl.
  exact H1.
Qed.

(* Inv_2 : les trains ne sont jamais au même emplacement. *)
Lemma PO_Safety_Inv_2: 
  forall (B:State),
    (* Invariants *)
    Inv_1 B
    -> Inv_2 B
    -> let B' := action B  in Inv_2 B'.
Proof.
  intros B H1 H2 B'.
  unfold B'. clear B'.
  unfold action.
  unfold Inv_2 in *.
  simpl.
  exact H2.	
Qed.

(* Inv_3a : le capteur 1 est actif ssi un train est à l'emplacement C1. *)
Lemma PO_Safety_Inv_3a: 
  forall (B:State),
    (* Invariants *)
    Inv_1 B
    -> Inv_2 B
    -> Inv_3a B
    -> Inv_3b B
    -> Inv_3c B
    -> let B' := action B  in Inv_3a B'.
Proof.
  intros B H1 H2 H3a H3b H3c B'.
  unfold B'. clear B'.
  unfold action.
  unfold Inv_3a in *.
  simpl.
  exact H3a.	
Qed.

(* Inv_3b : le capteur 2 est actif ssi un train est à l'emplacement C2. *)
Lemma PO_Safety_Inv_3b: 
  forall (B:State),
    (* Invariants *)
    Inv_1 B
    -> Inv_2 B
    -> Inv_3a B
    -> Inv_3b B
    -> Inv_3c B
    -> let B' := action B  in Inv_3b B'.
Proof.
  intros B H1 H2 H3a H3b H3c B'.
  unfold B'. clear B'.
  unfold action.
  unfold Inv_3b in *.
  simpl.
  exact H3b.	
Qed.

(* Inv_3c : le capteur 3 est actif ssi un train est à l'emplacement C3. *)
Lemma PO_Safety_Inv_3c: 
  forall (B:State),
    (* Invariants *)
    Inv_1 B
    -> Inv_2 B
    -> Inv_3a B
    -> Inv_3b B
    -> Inv_3c B
    -> let B' := action B  in Inv_3c B'.
Proof.
  intros B H1 H2 H3a H3b H3c B'.
  unfold B'. clear B'.
  unfold action.
  unfold Inv_3c in *.
  simpl.
  exact H3c.	
Qed.

(* == Globale == *)
Theorem PO_Safety:
  forall (B:State),
    (* Invariants *)
    Inv_1 B
    -> Inv_2 B
    -> Inv_3a B
    -> Inv_3b B
    -> Inv_3c B
    -> let B' := action B  in Inv_1 B' /\ Inv_2 B' /\ Inv_3a B' /\ Inv_3b B' /\ Inv_3c B'.
Proof.
  intro B.
  split.
  - apply PO_Safety_Inv_1 ; assumption.
  - split.
    + apply PO_Safety_Inv_2 ; assumption.
    + split.
      * apply PO_Safety_Inv_3a ; assumption.
      * split.
        apply PO_Safety_Inv_3b ; assumption.
        apply PO_Safety_Inv_3c ; assumption.
Qed.


End Stop.
(*### END : Stop ###*)




(*### Evenement : Avance ###*)
Module Avance.
 
(* == Action == *) 
(* déplace le train t1 *)
Definition action_t1 (B:State) : State :=
  let t1_start : bool := 
      match B.(emplacement_t1) with
        | C1 => negb B.(actif_c2)
        | C2 => negb B.(actif_c3)
        | C3 => negb B.(actif_c1)
      end in
  
  let futur_emplacement_t1 : emplacement := if t1_start then (next B.(emplacement_t1)) else B.(emplacement_t1) in
  
  let etat_c1 : bool := equalEmplacement futur_emplacement_t1 C1 in
  let etat_c2 : bool := equalEmplacement futur_emplacement_t1 C2 in
  let etat_c3 : bool := equalEmplacement futur_emplacement_t1 C3 in
  
  mkState etat_c1 etat_c2 etat_c3 t1_start B.(roule_t2) futur_emplacement_t1 B.(emplacement_t2).

(* déplace le train t2 *)
Definition action_t2 (B:State) : State :=
  let t2_start : bool := 
      match B.(emplacement_t2) with
        | C1 => negb B.(actif_c2)
        | C2 => negb B.(actif_c3)
        | C3 => negb B.(actif_c1)
      end in
  
  let futur_emplacement_t2 : emplacement := if t2_start then (next B.(emplacement_t2)) else B.(emplacement_t2) in
  
  let etat_c1 : bool := equalEmplacement futur_emplacement_t2 C1 in
  let etat_c2 : bool := equalEmplacement futur_emplacement_t2 C2 in
  let etat_c3 : bool := equalEmplacement futur_emplacement_t2 C3 in
  
  mkState etat_c1 etat_c2 etat_c3 B.(roule_t1) t2_start B.(emplacement_t1) futur_emplacement_t2.

(* Déplace les 2 trains *)
(* L'action qui réunit action_t1 et action_t2 *)
Definition action (B:State) : State :=
  mkState 
    (orb (action_t1 B).(actif_c1) (action_t2 B).(actif_c1))
    (orb (action_t1 B).(actif_c2) (action_t2 B).(actif_c2))
    (orb (action_t1 B).(actif_c3) (action_t2 B).(actif_c3))
    (action_t1 B).(roule_t1)
                    (action_t2 B).(roule_t2)
                                    (action_t1 B).(emplacement_t1)
                                                    (action_t2 B).(emplacement_t2).


(* Inv_1 : il y a tjs un capteur inactif et deux actifs. *)
Lemma PO_Safety_Inv_1:
  forall (B:State),
    (* Invariants *)
    Inv_1 B
    -> Inv_2 B
    -> Inv_3a B
    -> Inv_3b B
    -> Inv_3c B
    -> let B' := action B in Inv_1 B'.
Proof.
  intros B H1 H2 H3a H3b H3c B'.
  unfold B'. clear B'.
  unfold action.
  unfold Inv_1.
  unfold Inv_2 in *.
  unfold Inv_3a in *.
  unfold Inv_3b in *.
  unfold Inv_3c in *.
  simpl.
  destruct emplacement_t1.
  - (* cas C1 *)
    destruct actif_c2.
    + (* cas true *)
      destruct emplacement_t2.
      * (* cas C1 *)
        discriminate H2. 
      * (* cas C2 *)
        destruct actif_c3.
        (* cas true *)
        discriminate H3c.
        (* cas false *)
        simpl.
        right.
        right.
        auto.
      * (* cas C3 *)
        destruct actif_c1.
        (* cas true *)
        discriminate H3b.
        (* cas false *)
        discriminate H3a.
    + (* cas false *)
      destruct emplacement_t2.
      * (* cas C1 *)
        discriminate H2.
      * (* cas C2 *)
        discriminate H3b.
      * (* cas C3 *)
        destruct actif_c1.
        (* cas true *)
        simpl.
        right.
        left.
        auto.
        (* cas false *)
        simpl.
        left.
        auto.
  - (* cas C2 *)
    destruct actif_c3.
    + (* cas true *)
      destruct emplacement_t2.
      * (* cas C1 *)
        discriminate H3c.
      * (* cas C2 *)
        discriminate H2.
      * (* cas C3 *)
        destruct actif_c1.
        (* cas true *)
        discriminate H3a.
        (* cas true *)
        simpl.
        left.
        auto.
    + (* cas false *)
      simpl.
      destruct emplacement_t2.
      * (* cas C1 *)
        destruct actif_c2.
        (* cas true *)
        simpl.
        right.
        right.
        auto.
        (* cas false *)
        discriminate H3b.
      * (* cas C2 *)
        discriminate H2.
      * (* cas C3 *)
        destruct actif_c1.
        (* cas true *)
        discriminate H3a.
        (* cas false *)
        simpl.
        right.
        right.
        auto.
  - (* cas C3 *)
    destruct actif_c1.
    + (* cas true *)
      destruct emplacement_t2.
      * (* cas C1 *)
        destruct actif_c2.
        (* cas true *)
        discriminate H3b.
        (* cas false *)
        simpl.
        right.
        left.
        auto.
      * (* cas C2 *)
        discriminate H3a.
      * (* cas C3 *)
        discriminate H2.
    + (* cas false *)
      destruct emplacement_t2.
      * (* cas C1 *)
        destruct actif_c2.
        (* cas true *)
        discriminate H3a.
        (* cas false *)
        simpl.
        left.
        auto.
      * (* cas C2 *)
        destruct actif_c3.
        (* cas true *)
        simpl.
        left.
        auto.
        (* cas false *)
        discriminate H3c.
      * (* cas C3 *)
        discriminate H2.
Qed.



(* Inv_2 : les trains ne sont jamais au même emplacement. *)
Lemma PO_Safety_Inv_2:
  forall (B:State),
    (* Invariants *)
    Inv_1 B
    -> Inv_2 B
    -> Inv_3a B
    -> Inv_3b B
    -> Inv_3c B
    -> let B' := action B in Inv_2 B'.
Proof.
  intros B H1 H2 H3a H3b H3c B'.
  unfold B'. clear B'.
  unfold action.
  unfold Inv_2 in *.
  unfold Inv_1 in *.
  unfold Inv_3a in *.
  unfold Inv_3b in *.
  unfold Inv_3c in *.
  simpl.
  destruct emplacement_t1.
  - (* cas t1 = C1 *)
    destruct actif_c2.
    + (* cas true *)
      destruct emplacement_t2.
      * (* cas t2 = C1 *)
        discriminate H2.
      * (* cas t2 = C2 *)
        destruct actif_c3.
        (* cas true *)
        simpl.
        exact H2.
        (* cas false *)
        simpl.
        auto.
      * (* cas t2 = C3 *)
        destruct actif_c1.
        (* cas true *)
        simpl.
        exact H2.
        (* cas false *)
        discriminate H3a.
    + (* cas false *)
      destruct emplacement_t2.
      * (* cas t2 = C1 *)
        discriminate H2.
      * (* cas t2 = C2 *)
        discriminate H3b.
      * (* cas t2 = C3 *)
        destruct actif_c1.
        (* cas true *)
        simpl.
        auto.
        (* cas false *)
        discriminate H3a.
  - (* cas t1 = C2 *)
    destruct actif_c3.
    + (* cas true *)
      destruct emplacement_t2.
      * (* cas t2 = C1 *)
        discriminate H3c.
      * (* cas t2 = C2 *)
        discriminate H2.
      * (* cas t2 = C3 *)
        destruct actif_c1.
        (* cas true *)
        discriminate H3a.
        (* cas false *)
        simpl.
        auto.
    + (* cas false *)
      destruct emplacement_t2.
      * (* cas t2 = C1 *)
        destruct actif_c2.
        (* cas true *)
        simpl.
        auto.
        (* cas false *)
        discriminate H3b.
      * (* cas t2 = C2 *)
        discriminate H2.
      * (* cas t2 = C3 *)
        discriminate H3c.
  - (* cas t1 = C3 *)
    destruct actif_c1.
    + (* cas true *)
      destruct emplacement_t2.
      * (* cas t2 = C1 *)
        destruct actif_c2.
        (* cas true *)
        discriminate H3b.
        (* cas false *)
        simpl.
        auto.
      * (* cas t2 = C2 *)
        discriminate H3a.
      * (* cas t2 = C3 *)
        discriminate H2.
    + (* cas false *)
      destruct emplacement_t2.
      * (* cas t2 = C1 *)
        discriminate H3a.
      * (* cas t2 = C2 *)
        destruct actif_c3.
        (* cas true *)
        simpl.
        auto.
        (* cas false *)
        discriminate H3c.
      * (* cas t2 = C3 *)
        discriminate H2.
Qed.


(* Inv_3a : le capteur 1 est actif ssi un train est à l'emplacement C1. *)
Lemma PO_Safety_Inv_3a:
  forall (B:State),
    (* Invariants *)
    Inv_1 B
    -> Inv_2 B
    -> Inv_3a B
    -> Inv_3b B
    -> Inv_3c B
    -> let B' := action B in Inv_3a B'.
Proof.
  intros B H1 H2 H3a H3b H3c B'.
  unfold B'. clear B'.
  unfold action.
  unfold Inv_2 in *.
  unfold Inv_1 in *.
  unfold Inv_3a in *.
  unfold Inv_3b in *.
  unfold Inv_3c in *.
  simpl.
  destruct emplacement_t1.
  - (* cas t1 = C1 *)
    destruct actif_c2.
    + (* cas true *)
      destruct emplacement_t2.
      * (* cas t2 = C1 *)
        discriminate H2.
      * (* cas t2 = C2 *)
        destruct actif_c3.
        (* cas true *)
        discriminate H3c.
        (* cas false *)
        simpl.
        reflexivity.
      * (* cas t2 = C3 *)
        discriminate H3b.
    + (* cas false *)
      destruct emplacement_t2.
      * (* cas t2 = C1 *)
        discriminate H2.
      * (* cas t2 = C2 *)
        discriminate H3b.
      * (* cas t2 = C3 *)
        destruct actif_c1.
        (* cas true *)
        simpl.
        exact H2.
        (* cas false *)
        discriminate H3a.
  - (* cas t1 = C2 *)
    destruct actif_c3.
    + (* cas true *)
      destruct emplacement_t2.
      * (* cas t2 = C1 *)
        discriminate H3c.
      * (* cas t2 = C2 *)
        discriminate H2.
      * (* cas t2 = C3 *)
        destruct actif_c1.
        (* cas true *)
        simpl.
        auto.
        (* cas false *)
        simpl.
        auto.
    + (* cas false *)
      destruct emplacement_t2.
      * (* cas t2 = C1 *)
        destruct actif_c2.
        (* cas true *)
        simpl.
        auto.
        (* cas false *)
        discriminate H3b.
      * (* cas t2 = C2 *)
        discriminate H2.
      * (* cas t2 = C3 *)
        discriminate H3c.
  - (* cas t1 = C3 *)
    destruct actif_c1.
    + (* cas true *)
      destruct emplacement_t2.
      * (* cas t2 = C1 *)
        destruct actif_c2.
        (* cas true *)
        discriminate H3b.
        (* cas false *)
        simpl.
        auto.
      * (* cas t2 = C2 *)
        discriminate H3a.
      * (* cas t2 = C3 *)
        discriminate H2.
    + (* cas false *)
      destruct emplacement_t2.
      * (* cas t2 = C1 *)
        destruct actif_c2.
        (* cas true *)
        discriminate H3b.
        (* cas false *)
        simpl.
        reflexivity.
      * (* cas t2 = C2 *)
        simpl.
        reflexivity.
      * (* cas t2 = C3 *)
        discriminate H2.
Qed.



(* Inv_3b : le capteur 2 est actif ssi un train est à l'emplacement C2. *)
Lemma PO_Safety_Inv_3b:
  forall (B:State),
    (* Invariants *)
    Inv_1 B
    -> Inv_2 B
    -> Inv_3a B
    -> Inv_3b B
    -> Inv_3c B
    -> let B' := action B in Inv_3b B'.
Proof.
  intros B H1 H2 H3a H3b H3c B'.
  unfold B'. clear B'.
  unfold action.
  unfold Inv_2 in *.
  unfold Inv_1 in *.
  unfold Inv_3a in *.
  unfold Inv_3b in *.
  unfold Inv_3c in *.
  simpl.
  destruct emplacement_t1.
  - (* cas t1 = C1 *)
    destruct actif_c2.
    + (* cas true *)
      destruct emplacement_t2.
      * (* cas t2 = C1 *)
        discriminate H2.
      * (* cas t2 = C2 *)
        destruct actif_c3.
        (* cas true *)
        discriminate H3c.
        (* cas false *)
        simpl.
        reflexivity.
      * (* cas t2 = C3 *)
        discriminate H3b.
    + (* cas false *)
      destruct emplacement_t2.
      * (* cas t2 = C1 *)
        discriminate H2.
      * (* cas t2 = C2 *)
        discriminate H3b.
      * (* cas t2 = C3 *)
        destruct actif_c1.
        (* cas true *)
        simpl.
        reflexivity.
        (* cas false *)
        discriminate H3a.
  - (* cas t1 = C2 *)
    destruct actif_c3.
    + (* cas true *)
      destruct emplacement_t2.
      * (* cas t2 = C1 *)
        discriminate H3c.
      * (* cas t2 = C2 *)
        discriminate H2.
      * (* cas t2 = C3 *)
        destruct actif_c1.
        (* cas true *)
        simpl.
        auto.
        (* cas false *)
        simpl.
        auto.
    + (* cas false *)
      destruct emplacement_t2.
      * (* cas t2 = C1 *)
        destruct actif_c2.
        (* cas true *)
        simpl.
        exact H2.
        (* cas false *)
        discriminate H3b.
      * (* cas t2 = C2 *)
        discriminate H2.
      * (* cas t2 = C3 *)
        discriminate H3c.
  - (* cas t1 = C3 *)
    destruct actif_c1.
    + (* cas true *)
      destruct emplacement_t2.
      * (* cas t2 = C1 *)
        destruct actif_c2.
        (* cas true *)
        discriminate H3b.
        (* cas false *)
        simpl.
        auto.
      * (* cas t2 = C2 *)
        discriminate H3a.
      * (* cas t2 = C3 *)
        discriminate H2.
    + (* cas false *)
      destruct emplacement_t2.
      * (* cas t2 = C1 *)
        destruct actif_c2.
        (* cas true *)
        discriminate H3b.
        (* cas false *)
        simpl.
        reflexivity.
      * (* cas t2 = C2 *)
        destruct actif_c3.
        (* cas true *)
        simpl.
        auto.
        (* cas false *)
        discriminate H3c.
      * (* cas t2 = C3 *)
        discriminate H2.
Qed.



(* Inv_3c : le capteur 3 est actif ssi un train est à l'emplacement C3. *)
Lemma PO_Safety_Inv_3c:
  forall (B:State),
    (* Invariants *)
    Inv_1 B
    -> Inv_2 B
    -> Inv_3a B
    -> Inv_3b B
    -> Inv_3c B
    -> let B' := action B in Inv_3c B'.
Proof.
  intros B H1 H2 H3a H3b H3c B'.
  unfold B'. clear B'.
  unfold action.
  unfold Inv_2 in *.
  unfold Inv_1 in *.
  unfold Inv_3a in *.
  unfold Inv_3b in *.
  unfold Inv_3c in *.
  simpl.
  destruct emplacement_t1.
  - (* cas t1 = C1 *)
    destruct actif_c2.
    + (* cas true *)
      destruct emplacement_t2.
      * (* cas t2 = C1 *)
        discriminate H2.
      * (* cas t2 = C2 *)
        destruct actif_c3.
        (* cas true *)
        discriminate H3c.
        (* cas false *)
        simpl.
        reflexivity.
      * (* cas t2 = C3 *)
        discriminate H3b.
    + (* cas false *)
      destruct emplacement_t2.
      * (* cas t2 = C1 *)
        discriminate H2.
      * (* cas t2 = C2 *)
        discriminate H3b.
      * (* cas t2 = C3 *)
        destruct actif_c1.
        (* cas true *)
        simpl.
        auto.
        (* cas false *)
        discriminate H3a.
  - (* cas t1 = C2 *)
    destruct actif_c3.
    + (* cas true *)
      destruct emplacement_t2.
      * (* cas t2 = C1 *)
        discriminate H3c.
      * (* cas t2 = C2 *)
        discriminate H2.
      * (* cas t2 = C3 *)
        destruct actif_c1.
        (* cas true *)
        simpl.
        auto.
        (* cas false *)
        simpl.
        auto.
    + (* cas false *)
      destruct emplacement_t2.
      * (* cas t2 = C1 *)
        destruct actif_c2.
        (* cas true *)
        simpl.
        auto.
        (* cas false *)
        discriminate H3b.
      * (* cas t2 = C2 *)
        discriminate H2.
      * (* cas t2 = C3 *)
        discriminate H3c.
  - (* cas t1 = C3 *)
    destruct actif_c1.
    + (* cas true *)
      destruct emplacement_t2.
      * (* cas t2 = C1 *)
        destruct actif_c2.
        (* cas true *)
        discriminate H3b.
        (* cas false *)
        simpl.
        auto.
      * (* cas t2 = C2 *)
        discriminate H3a.
      * (* cas t2 = C3 *)
        discriminate H2.
    + (* cas false *)
      destruct emplacement_t2.
      * (* cas t2 = C1 *)
        destruct actif_c2.
        (* cas true *)
        discriminate H3b.
        (* cas false *)
        simpl.
        reflexivity.
      * (* cas t2 = C2 *)
        destruct actif_c3.
        (* cas true *)
        simpl.
        exact H2.
        (* cas false *)
        discriminate H3c.
      * (* cas t2 = C3 *)
        discriminate H2.
Qed.


(* == Globale == *)
Theorem PO_Safety:
  forall (B:State),
    (* Invariants *)
    Inv_1 B
    -> Inv_2 B
    -> Inv_3a B
    -> Inv_3b B
    -> Inv_3c B
    -> let B' := action B  in Inv_1 B' /\ Inv_2 B' /\ Inv_3a B' /\ Inv_3b B' /\ Inv_3c B'.
Proof.
  intro B.
  split.
  - apply PO_Safety_Inv_1 ; assumption.
  - split.
    + apply PO_Safety_Inv_2 ; assumption.
    + split.
      * apply PO_Safety_Inv_3a ; assumption.
      * split. 
        apply PO_Safety_Inv_3b ; assumption.
        apply PO_Safety_Inv_3c ; assumption.
Qed.


End Avance.
(*### END : Avance ###*)


End Tchou.
(** END :  Machine Tchou **)
