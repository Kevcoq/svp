
Require Import Arith.
Require Import List.


(*-----------------------------------------------------------*)

(**  * Fonctions calculables vs. relations logiques *)

Fixpoint sum_fun (n:nat) : nat :=
  match n with
    | 0 => 0
    | S m => n + sum_fun m
  end.

Example sum_fun_5:
  sum_fun 5 = 15.
Proof.
  compute. reflexivity.
Qed.

Example sum_fun_10:
  sum_fun 10 = 55.
Proof.
  compute. reflexivity.
Qed.

Lemma two_times:
  forall n : nat, 2 * n = n + n.
Proof.
  intro n. ring.
Qed.

Theorem sum_fun_Gauss:
  forall n : nat, 2 * (sum_fun n) = n * (n + 1).
Proof.
  intro n.
  rewrite two_times.
  induction n as [| n'].
  - (* cas n=0 *)
    simpl.
    reflexivity.
  - (* cas n=S n' *)
    simpl.    
    (* SearchPattern (S _ = S _).
       eq_S: forall x y : nat, x = y -> S x = S y *)
    apply eq_S.
    assert (H:  n' + sum_fun n' + S (n' + sum_fun n')
                = sum_fun n' + sum_fun n' + n' + S n').
    { ring. }
    rewrite H. clear H.
    rewrite IHn'.
    ring.
Qed.


(*  Règles : 

                             n s : nat   sum_rel n s
   ============= (sum_O)    ============================ (sum_S)
    sum_rel 0 0              sum_rel (S n) ( (S n) + s)

*)

Inductive sum_rel : nat -> nat -> Set :=
| sum_O: sum_rel 0 0
| sum_S: forall n s:nat,
           sum_rel n s -> sum_rel (S n) ( (S n) + s).

Example sum_rel_3:
  sum_rel 3 6.
Proof.
  change (sum_rel 3 (3 + 3)).
  apply sum_S.
  change (sum_rel 2 (2 + 1)).
  apply sum_S.
  change (sum_rel 1 (1 + 0)).
  apply sum_S.
  apply sum_O.
Qed.

Theorem sum_rel_Gauss:
  forall n s : nat, (sum_rel n s) -> 2 * s = n * (n + 1).
Proof.
  induction n as [|n'].
  - (* cas n=0 *)
    intros s Hsum.
    inversion Hsum.
    simpl.
    reflexivity.
  - (* cas n=S n' *)
    intros s Hsum.
    inversion Hsum. clear Hsum n H H1.
    apply IHn' in H0. clear IHn'.
    rewrite two_times in *.
    assert (H1: S n' + s0 + (S n' + s0) = s0 + s0 + S n' + S n').
    { ring. }
    rewrite H1. clear H1.
    rewrite H0.
    ring.
Qed.

Theorem sum_rel_Gauss':
  forall n s : nat, (sum_rel n s) -> 2 * s = n * (n + 1).
Proof.
  intros n s H.
  induction H.  (* rule induction *)
  - (* cas sum_O *)
    simpl.
    reflexivity.
  - (* cas sum_S *)
    rewrite two_times in *.
    assert (H1: S n + s + (S n + s) = s + s + S n + S n).
    { ring. }
    rewrite H1. clear H1.
    rewrite IHsum_rel.
    ring.
Qed.


(** ** Exercice 1 *)

(** *** Question 1 *)
Fixpoint concat_fun {A : Set} (l1 : list A) (l2 : list A) : list A :=
  match l1 with 
      nil => l2
    | cons h q => cons h (concat_fun q l2)
  end.


(** *** Question 2 *)
Inductive concat_rel (A : Set) : list A -> list A -> list A -> Set :=
| concat_nil: forall l : list A, (concat_rel A nil l l)
| concat_cons: forall h : A, forall l1 : list A, forall l2: list A, forall l3 : list A,
                 concat_rel A l1 l2 l3 -> concat_rel A (cons h l1) l2 (cons h l3).


(** *** Question 3 *)

Lemma concat_fun_cons:
  forall A : Set, forall e : A, forall l1 l2 : list A,
    concat_fun (e::l1) l2 = e::(concat_fun l1 l2).
Proof.
  intros A e l1 l2.
  case l1.
  - (* cas nil *)
    simpl.
    reflexivity.
  - (* cas cons *)
    simpl.
    reflexivity.
Qed.


(** *** Question 4 *)
Lemma concat_rel_cons:
  forall A : Set, forall e : A, forall l1 l2 l3 : list A,
    concat_rel A l1 l2 l3 -> concat_rel A (e::l1) l2 (e::l3).
Proof.
  intros A e l1 l2 l3 relB.
  SearchPattern  ( concat_rel _ (_:: _) _ (_:: _) ).
  apply concat_cons.
  exact relB.
Qed.
    


(* <<<COMPLETER ICI>>> *)

(** *** Question 5 *)

Lemma concat_fun_length:
  forall A : Set, forall l1 l2 : list A,
    length (concat_fun l1 l2) = (length l1) + (length l2).
Proof.
  intros A l1 l2.
  induction l1.
  - (* cas nil *)
    simpl.
    reflexivity.
  - (* cas cons *)
    simpl.
    rewrite IHl1.
    reflexivity.
Qed.



(** *** Question 6 *)

Lemma concat_rel_length:
  forall A : Set, forall l1 l2 l3 : list A,
    concat_rel A l1 l2 l3 
    -> length l3 = (length l1) + (length l2).
Proof.
  intros A l1 l2 l3 Hrel.
  induction Hrel.  (* rule induction *)
  - (* cas concat_nil *)
    simpl.
    reflexivity.
  - (* cas concat_cons *)
    simpl.
    rewrite IHHrel.
    reflexivity.
Qed.

(** ** Exercice 2 : appartenance à une liste *)

Section is_in.

Variable A : Set.
Variable A_eq_dec : forall a b : A, {a = b} + {a <> b}.

Fixpoint is_in_fun (e : A) (l : list A) : bool :=
  match l with
    | nil => false
    | e'::l' => match A_eq_dec e e' with
                  | left _ => true
                  | right _ => is_in_fun e l'
                end
  end.

End is_in.

Check is_in_fun.

Example is_in_fun_ex1:
  is_in_fun nat (eq_nat_dec) 3 (1::2::3::4::5::nil) = true.
Proof.
  compute. reflexivity.
Qed.

(** *** Question 1 *)

Inductive is_in_rel (A:Set) : A -> list A -> Prop :=
  is_in_head: forall x : A, forall l : list A, (is_in_rel A x (cons x l))
| is_in_tail: forall x : A, forall h : A, forall q : list A, 
                x <> h -> (is_in_rel A x q)
                -> (is_in_rel A x (cons h q)).
(** *** Question 2 *)
    
Example is_in_rel_ex1:
  is_in_rel nat 3 (1::2::3::4::5::nil).
Proof.
  apply is_in_tail.
  - auto with arith.
  - apply is_in_tail.
    + auto with arith.
    + apply is_in_head.
Qed.


(** ** Exercice 3 : liste préfixe *)

Section is_prefix.

Variable A : Set.
Variable A_eq_dec : forall a b : A, {a = b} + {a <> b}.

Fixpoint is_prefix_fun (l1 l2 : list A) : bool :=
  match l1 with
    | nil => true
    | e1::l1' => match l2 with
                   | nil => false
                   | e2::l2' => match A_eq_dec e1 e2 with
                                  | left _ => is_prefix_fun l1' l2'
                                  | right _ => false
                                end
                 end
  end.

End is_prefix.


(** *** Question 1 *)
(*  Règles : 

                                                Xs Ys : list A, x y : A, x = y, is_prefix_rel Xs Ys
   ===================== (is_prefix_nil)      ============================================== (is_prefix_cons)
    is_prefix_rel nil _                             is_prefix_rel (cons x Xs) (cons y Ys)

*)
Inductive is_prefix_rel (A:Set) : list A -> list A -> Prop :=
  is_prefix_nil: forall Ys : list A, (is_prefix_rel A nil Ys)
| is_prefix_cons: forall x : A, forall Xs Ys : list A, 
                    (is_prefix_rel A Xs Ys) -> 
                    (is_prefix_rel A (cons x Xs) (cons x Ys)).


(** *** Question 2 *)
Lemma is_prefix_is_in_fun: 
  forall A : Set, forall A_eq_dec : forall a b : A, {a = b} + {a <> b},
    forall l2 l1 : list A,
      (is_prefix_fun A A_eq_dec l1 l2) = true 
      -> (forall e :  A, is_in_fun A A_eq_dec e l1 = true -> is_in_fun A A_eq_dec e l2 = true).
Proof.
  intros A A_eq_dec.
  induction l2 as [| e2 l2'].
  - (* cas l2 = nil *)
    intros l1 is_prefix e is_in.
    case_eq l1.
    + (* cas l1 = nil *)
      intro l1N.
      rewrite l1N in is_in.
      exact is_in.
    + (* cas l1 != nil *)
      intros a l lCons.
      rewrite lCons in is_prefix.
      simpl in *.
      inversion is_prefix.
  - (* cas l2=e2::l2' *)
    intros l1 H1 e H2.
    simpl.
    destruct (A_eq_dec e e2).
    + (* cas e=e2 *)
      reflexivity.
    + (* cas e<>e2 *)
      { destruct l1 as [| e1 l1'].
        - (* cas l1=nil *)
          simpl in *.
          discriminate H2.
        - (* cas l1=e1::l1' *)
          { apply IHl2' with (l1:=l1').
            simpl in *.
            destruct (A_eq_dec e1 e2).
            + (* cas vrai *)
              exact H1.
            + (* cas false *)
              discriminate H1.
            + simpl in *.
              destruct (A_eq_dec e e1).
              * (* cas true *)
                rewrite <- e0 in H1.
                { destruct (A_eq_dec e e2).
                  + rewrite <- e3 in n. 
                    auto with arith.
                  + discriminate H1.
                }
              * (* cas false *)
                exact H2.
          }
      }
Qed.


(** *** Question 3 *)
Lemma is_prefix_is_in_rel: 
  forall A : Set,
  forall l1 l2 : list A,
    (is_prefix_rel A l1 l2)
    -> (forall e :  A, is_in_rel A e l1 -> is_in_rel A e l2).
Proof.
  intros A l1 l2 Hprefix.
  induction Hprefix.
  - (* cas nil *)
    intros e is_in.
    case_eq Ys.
    + (* cas nil *)
      intro YsNil.
      exact is_in.
    + (* cas !nil *)
      intros a l YsCons.
      inversion is_in.
  - (* cas !nil *) 
    intros e is_in.
    inversion is_in.
    apply is_in_head.
    apply is_in_tail.    
    exact H2.
    apply IHHprefix.
    exact H3.
Qed.