
(** Exemple du cours : Machine AbstractBridge et raffinement ConcreteBridge *)
(** Version avec exercices *)

Require Import Arith.

(* Utilitaires *)
Lemma pred_plus_S:
forall n m : nat,
  n > 0 -> pred n + S m = n + m.
Proof.
  destruct n as [|n'].
  - (* cas n=0 *)
    intros m Hcontra.
    inversion Hcontra.
  - (* cas n=S n' *)
    intros m Hdummy.
    clear Hdummy.
    simpl.
    SearchRewrite (S (_ + _)).
    (* plus_n_Sm: forall n m : nat, S (n + m) = n + S m *)
    rewrite plus_n_Sm.
    reflexivity.
Qed.

Lemma S_plus_pred:
forall n m : nat,
  m > 0 -> S n + pred m = n + m.
Proof.
  intros n m H.
  pattern (S n + pred m).
  rewrite plus_comm.
  pattern (n + m).
  rewrite plus_comm.
  apply pred_plus_S.
  exact H.
Qed.

Lemma gt_plus_l:
  forall n m p : nat,
    n > p -> n + m > p.
Proof.
  auto with arith.
Qed.
  
(** Machine abstraite *)
Module AbstractBridge.

Module Context.

(* Constante : nombre maximum de voitures *)
Parameter max_nb_cars : nat.

Axiom max_nb_cars_not_zero : max_nb_cars > O.

End Context.

Record State : Set := mkState {
  (* nombre de voitures entrée *)
  nb_cars_entered: nat
}.

(* Invariants *)
Definition Inv_1 (B:State) : Prop :=
 nb_cars_entered B <= Context.max_nb_cars.


(** Evénement : Init *)
Module Init.

Definition Guard (limit:nat) : Prop :=
  Context.max_nb_cars = limit.

Definition action (limit:nat) : State :=
  mkState 0.

Theorem PO_Safety:
  forall lim : nat,
    (* Guard *)
    Guard lim
    (* ==> post *)
    -> let B := action lim
       in Inv_1 B.
Proof.
  intros lim HGuard B.
  unfold B. clear B.
  unfold Inv_1 , action.
  simpl.
  SearchPattern ( 0 <= _ ).
  (* le_0_n: forall n : nat, 0 <= n *)
  apply le_0_n.
Qed.

End Init.


(** Evénement : CarEnterFromMainland *)
Module CarEnter.

Definition Guard (B:State) : Prop :=
  B.(nb_cars_entered) < Context.max_nb_cars.

Definition action (B:State) : State :=
  mkState (S B.(nb_cars_entered)).

Theorem PO_Safety:
  forall (B:State),
    (* Invariants *)
    Inv_1 B
    (* Guard *)
    -> Guard B
    (* ==> post *)
    -> let B' := action B
       in Inv_1 B'.
Proof.
  intros B HInv_1 HGuard B'.
  unfold B'. clear B'.
  unfold Inv_1 , action.
  simpl.
  unfold Guard in HGuard.
  SearchPattern ( ?X < ?Y -> S ?X <= ?Y ).
  (* lt_le_S: forall n m : nat, n < m -> S n <= m *)
  apply lt_le_S.
  exact HGuard.
Qed.

End CarEnter.


(** Evénement : CarLeave *)
Module CarLeave.

Definition Guard (B:State) : Prop :=
  B.(nb_cars_entered) > 0.

Definition action (B:State) : State :=
  mkState (pred B.(nb_cars_entered)).

Theorem PO_Safety:
  forall (B:State),
    (* Invariants *)
    Inv_1 B
    (* Guard *)
    -> Guard B
    (* ==> post *)
    -> let B' := action B
       in Inv_1 B'.
Proof.
  intros B HInv_1 HGuard B'.
  unfold B'. clear B'.
  unfold Inv_1 , action in *.
  simpl.
  unfold Guard in HGuard.
  destruct (nb_cars_entered B) as [| nb_entered].
  - (* cas 0 *)
    inversion HGuard. (* contradiction *)
  - (* cas S nb_entered *)
    simpl.
    SearchPattern ( S ?X <= ?Y -> ?X <= ?Y ).
    (* le_Sn_le: forall n m : nat, S n <= m -> n <= m *)
    apply le_Sn_le.
    exact HInv_1.
Qed.

End CarLeave.


Theorem PO_Deadlock_Freedom:
  forall (B:State),
    (* Invariants *)
    Inv_1 B
    (* Guard disjunction *)
    -> CarEnter.Guard B
       \/ CarLeave.Guard B.
Proof.
  intros B HInv_1.
  unfold Inv_1 in HInv_1.
  unfold CarEnter.Guard, CarLeave.Guard.
  destruct (nb_cars_entered B) as [|nb_entered].
  - (* cas 0 *)
    assert (HAxiom: Context.max_nb_cars > 0).
    apply Context.max_nb_cars_not_zero.
    unfold gt in HAxiom.
    left.
    exact HAxiom.
  - (* cas S (nb_entered) *)
    right.
    SearchPattern (S _ > 0).
    (* gt_Sn_O: forall n : nat, S n > 0 *)
    apply gt_Sn_O.
Qed.

End AbstractBridge.


(* Machine concrete *)
Module ConcreteBridge.

(* Contexte : constantes et axiomes *)
Module Context.

(* Constante par raffinement : nombre maximum de voitures *)
Definition max_nb_cars : nat := AbstractBridge.Context.max_nb_cars.

Lemma max_nb_cars_not_zero: 
  max_nb_cars > 0.
Proof.
  unfold max_nb_cars.
  apply AbstractBridge.Context.max_nb_cars_not_zero.
Qed.
  
End Context.

(* Etat concret de la machine *)
Record State : Set := mkState {

  (* nombre de voitures vers l'île *)
  nb_cars_to_island: nat;
  (* nombre de voitures vers le continent *)
  nb_cars_to_mainland: nat;
  (* nombre de voitures sur l'île *)
  nb_cars_on_island: nat
                        
}.


(* définition annexe : nombre total de voitures *)
Definition total_nb_cars (B:State) : nat :=
  B.(nb_cars_to_island)
  + B.(nb_cars_to_mainland)
  + B.(nb_cars_on_island).


(* Invariants *)
(* Invariants de glue *)
Definition Glue_1  (B:State) (AB:AbstractBridge.State) : Prop :=
  total_nb_cars B = AbstractBridge.nb_cars_entered AB.
  
Definition Inv_1 (B:State) : Prop :=
  B.(nb_cars_to_island) = 0
  \/ B.(nb_cars_to_mainland) = 0.


(** Evénement : Init *)
Module Init. (* raffinement de AbstractBridge.Init *)

Definition Guard (limit:nat) : Prop :=
  Context.max_nb_cars = limit.

Definition action (limit:nat) : State :=
  mkState 0 0 0.

Theorem PO_Strengthening:
  (* Paramètres *)
  forall lim : nat,
    (* Garde concrète *)
    Guard lim
    (* ====>*)
    (* Garde abstraite *)
    -> AbstractBridge.Init.Guard lim.
Proof.
  intros lim HGuard .
  unfold AbstractBridge.Init.Guard.
  unfold Guard in HGuard.
  unfold Context.max_nb_cars in HGuard.
  exact HGuard.
Qed.

Theorem PO_Safety:
  forall lim : nat,
    (* Guard *)
    Guard lim
    (* ==> post *)
    -> let B := action lim
       in Inv_1 B.
Proof.
  intros lim HGuard B.
  unfold B. clear B.
  unfold Inv_1 , action.
  unfold total_nb_cars.
  simpl.
  left.
  reflexivity.
Qed.

Theorem PO_Simulation:
  (* Paramètres *)
  forall lim : nat,
    (* Gardes concrètes *)
    Guard lim
    (* post *)
    -> let B := action lim in
       let AB := AbstractBridge.Init.action lim 
       in 
       (* ====> invariants de glue *) 
       Glue_1 B AB.
Proof.
  intros lim HGuard B AB.
  unfold B in *. clear B.
  unfold AB in *. clear AB.
  unfold action , AbstractBridge.Init.action.
  unfold Glue_1.
  unfold total_nb_cars.
  simpl.
  reflexivity.
Qed.

End Init.


(** Evénement : CarEnterFromMainland *)
Module CarEnterFromMainland. (* raffinement de AbstractBridge.CarEnter *)

Definition Guard (B:State) : Prop :=
  B.(nb_cars_to_mainland) = 0 
  /\ B.(nb_cars_to_island) + B.(nb_cars_on_island) < Context.max_nb_cars.

Definition action (B:State) : State :=
  mkState 
    (S B.(nb_cars_to_island))
    B.(nb_cars_to_mainland)
        B.(nb_cars_on_island).

Theorem PO_Strengthening:
  forall B : State, forall AB : AbstractBridge.State,
    (* Invariants abstraits *)
    AbstractBridge.Inv_1 AB
    (* Invariants de glue *)
    -> Glue_1 B AB
    (* Invariants concrets *)
    -> Inv_1 B 
    (* Garde concrète *)
    -> Guard B
    (* ==> post *)
    -> AbstractBridge.CarEnter.Guard AB.
Proof.
  intros B AB HAbsInv_1 HGlue_1 HInv_1 HGuard.
  unfold AbstractBridge.CarEnter.Guard.
  unfold Glue_1 in HGlue_1.
  unfold AbstractBridge.Inv_1 in HAbsInv_1.
  unfold Guard in HGuard.
  destruct HGuard as [HGuard1 HGuard2].
  rewrite <- HGlue_1 in *.
  unfold total_nb_cars.
  rewrite HGuard1.
  rewrite <- plus_assoc.
  rewrite plus_O_n.
  unfold Context.max_nb_cars in HGuard2.
  exact HGuard2.
Qed.

Theorem PO_Safety:
  forall (B:State), forall (AB:AbstractBridge.State),
    (* Invariants abstraits *)
    AbstractBridge.Inv_1 AB
    (* Invariants de glue *)
    -> Glue_1 B AB
    (* Invariants concrets *)
    -> Inv_1 B
    (* Garde concrète *)
    -> Guard B
    (* ==> post *)
    -> let B' := action B
       in Inv_1 B'.
Proof.
  intros B AB HAbsInv_1 HGlue_1 HInv_1 HGuard B'.
  unfold B'. clear B'.
  unfold Inv_1 , action.
  simpl.
  unfold Guard in HGuard.
  destruct HGuard as [HGuard1 HGuard2].
  right.
  exact HGuard1.
Qed.  

Theorem PO_Simulation:
  forall (B:State), forall (AB:AbstractBridge.State),
    (* Invariants abstraits *)
    AbstractBridge.Inv_1 AB
    (* Invariants de glue *)
    -> Glue_1 B AB
    (* Invariants concrets *)
    -> Inv_1 B
    (* Garde concrète *)
    -> Guard B
    (* ==> post *)
    -> let B' := action B
       in let AB' := AbstractBridge.CarEnter.action AB
          in (* ====> Invariants de glue *)
          Glue_1 B' AB'.
Proof.
  intros B AB HAbsInv_1 HGlue_1 HInv_1 HGuard B' AB'.
  unfold B'. clear B'.
  unfold AB'. clear AB'.
  unfold action , AbstractBridge.CarEnter.action.
  unfold Glue_1 , total_nb_cars in *.
  simpl in *.
  SearchPattern ( S _ =  S _ ).
  (* eq_S: forall x y : nat, x = y -> S x = S y *)
  apply eq_S.
  exact HGlue_1.
Qed.

End CarEnterFromMainland.



(** Evénement : CarLeaveToIsland *)
Module CarLeaveToIsland. (* Nouvel événement *)

Definition Guard (B:State) : Prop :=
  B.(nb_cars_to_island) > 0.

Definition action (B:State) : State :=
  mkState
     (pred B.(nb_cars_to_island))
    B.(nb_cars_to_mainland)
        (S B.(nb_cars_on_island)).

Theorem PO_Safety:
  forall (B:State) (AB:AbstractBridge.State),
    (* Invariants abstraits *)
    AbstractBridge.Inv_1 AB
    (* Invariants de Glue *)
    -> Glue_1 B AB
    (* Invariants concrets *)
    -> Inv_1 B
    (* Guard *)
    -> Guard B
    (* ==> post *)
    -> let B' := action B
       in Inv_1 B'.
Proof.
  intros B AB HAbsInv_1 HGlue_1 HInv_1 HGuard B'.
  unfold B'. clear B'.
  unfold Inv_1, action in *.
  simpl.
  unfold Guard in HGuard.
  destruct (nb_cars_to_island B).
  - (* cas 0 *)
    inversion HGuard.
  - (* cas S n *)
    clear HGuard.
    simpl.
    destruct HInv_1 as [HInv_1 | HInv_2].
    + (* cas left *)
      inversion HInv_1.
    + (* cas right *)
      right.
      exact HInv_2.
Qed.

Theorem PO_Simulation:
  forall (B:State), forall (AB:AbstractBridge.State),
    (* Invariants abstraits *)
    AbstractBridge.Inv_1 AB
    (* Invariants de glue *)
    -> Glue_1 B AB
    (* Invariants concrets *)
    -> Inv_1 B
    (* Garde concrète *)
    -> Guard B
    (* ==> post *)
    -> let B' := action B
       in (* ====> Invariants de glue *)
       Glue_1 B' AB.
Proof.
  intros B AB HAbsInv_1 HGlue_1 HInv_1 HGuard B'.
  unfold B'. clear B'.
  unfold Glue_1 in *.
  unfold action.
  unfold total_nb_cars in *.
  simpl.
  unfold Guard in HGuard. 
  unfold Inv_1 in HInv_1.
  destruct HInv_1 as [HInv_1 | HInv_1].
  - (* cas left *)
    rewrite HInv_1 in HGuard.
    inversion HGuard.
  - (* cas right *)
    rewrite HInv_1 in *.
    rewrite <- plus_assoc in *.
    SearchRewrite (0 + _).
    (* plus_O_n: forall n : nat, 0 + n = n *)
    rewrite plus_O_n in *.
    rewrite <- HGlue_1.
    apply pred_plus_S.
    exact HGuard.
Qed.
    
(*  Convergence : PO obligatoire pour nouvel événement *)

Definition variant (B:State) : nat :=
  B.(nb_cars_to_island).

Theorem PO_Convergence:
  (* Etat courant *)
  forall B : State, forall AB : AbstractBridge.State,
    (* Invariants abstraits *)
    AbstractBridge.Inv_1 AB
    (* Invariants de glue *)
    -> Glue_1 B AB
    (* Invariants concrets *)
    -> Inv_1 B
    (* Guard *)
    -> Guard B
    (* Etat successeur *)
    -> let B' := action B
       in 
       (* Décroissance *)
       variant B' < variant B.
Proof.
  intros B AB HAbsInv_1 HGlue_1 HInv_1 HGuard B'.
  unfold B'. clear B'.
  unfold variant , action.
  simpl.
  unfold Guard in HGuard.
  unfold gt in HGuard.
  SearchPattern ( pred ?X < ?X ).
  (* lt_pred_n_n: forall n : nat, 0 < n -> pred n < n *)
  apply lt_pred_n_n.
  exact HGuard.
Qed.

End CarLeaveToIsland.

(** Evénement : CarEnterFromIsland *)

Module CarEnterFromIsland. (* nouvel événement *)

Definition Guard (B:State) : Prop :=
  B.(nb_cars_to_island)  = 0 /\ B.(nb_cars_on_island) + B.(nb_cars_to_mainland) < Context.max_nb_cars /\ B.(nb_cars_on_island) > 0.

Definition action (B:State) : State :=
  mkState 
    B.(nb_cars_to_island)
    (S B.(nb_cars_to_mainland))
      (pred B.(nb_cars_on_island)).

Theorem PO_Safety:
  forall B : State, forall AB : AbstractBridge.State, 
    (* Invariants abstraits *)
    AbstractBridge.Inv_1 AB
    (* Invariants de Glue *)
    -> Glue_1 B AB
    (* Invariants concrets *)
    -> Inv_1 B
    (* Guard *)
    -> Guard B
    (* ==> post *)
    -> let B' := action B
       in Inv_1 B'.
Proof.
  intros B AB ABHI1 Gl1 I1 Gu1 B'.
  unfold B'. clear B'.
  unfold Inv_1, action.
  simpl.
  left.
  unfold Guard in *.
  destruct Gu1.
  exact H.
Qed.




Theorem PO_Simulation:
  forall (B:State), forall (AB:AbstractBridge.State),
    (* Invariants abstraits *)
    AbstractBridge.Inv_1 AB
    (* Invariants de glue *)
    -> Glue_1 B AB
    (* Invariants concrets *)
    -> Inv_1 B
    (* Garde concrète *)
    -> Guard B
    (* ==> post *)
    -> let B' := action B
       in (* ====> Invariants de glue *)
       Glue_1 B' AB.
Proof.
  intros B AB ABHI1 Gl1 HI1 Gu1 B'.
  unfold B'. clear B'.
  unfold Glue_1, action.
  unfold total_nb_cars.
  simpl.
  SearchRewrite( S _ + pred _).
  (* S_plus_pred: forall n m : nat, m > 0 -> S n + pred m = n + m *)
  SearchPattern( ?X + ?Y + ?Z = ?X + (?Y + ?Z)).
  (* plus_assoc_reverse: forall n m p : nat, n + m + p = n + (m + p) *)
  rewrite plus_assoc_reverse.
  rewrite S_plus_pred.
  -
    unfold Glue_1 in *.
    unfold total_nb_cars in Gl1.
    SearchPattern( ?X + (?Y + ?Z) = ?X + ?Y + ?Z).
    (* plus_assoc: forall n m p : nat, n + (m + p) = n + m + p *)
    rewrite plus_assoc.
    exact Gl1.
  -
    unfold Guard in *.
    destruct Gu1.
    destruct H0.
    exact H1.
Qed.



  
(*  Convergence : PO obligatoire pour nouvel événement *)
Definition variant (B:State) : nat :=
  B.(nb_cars_on_island).


Theorem PO_Convergence:
  (* Etat courant *)
  forall B : State, forall AB : AbstractBridge.State,
    (* Invariants abstraits *)
    AbstractBridge.Inv_1 AB
    (* Invariants de Glue *)
    -> Glue_1 B AB
    (* Invariants concrets *)
    -> Inv_1 B
    (* Guard *)
    -> Guard B
    (* Etat successeur *)
    -> let B' := action B
       in 
       (* Décroissance *)
       variant B' < variant B.
Proof.
  intros A AB ABI1 Gl1 I1 Gu1 B'.
  unfold B'. clear B'.
  unfold variant, action.
  simpl.
  SearchPattern( pred ?X < ?X).
  (* lt_pred_n_n: forall n : nat, 0 < n -> pred n < n *)
  apply lt_pred_n_n.
  unfold Guard in *.
  destruct Gu1.
  destruct H0.
  unfold gt in H1.
  exact H1.
Qed.

End CarEnterFromIsland.



(** Evénement : CarLeaveToMainland *)
Module CarLeaveToMainland.  (* raffinement de CarLeave *)

Definition Guard (B:State) : Prop :=
  B.(nb_cars_to_island) = 0 /\ B.(nb_cars_to_mainland) > 0
  /\ B.(nb_cars_to_mainland) + B.(nb_cars_on_island) < Context.max_nb_cars.
  

Definition action (B:State) : State :=
  mkState 
    B.(nb_cars_to_island)
        (pred B.(nb_cars_to_mainland))
        B.(nb_cars_on_island).


Theorem PO_Strengthening:
  forall B : State, forall AB : AbstractBridge.State,
    (* Invariants abstraits *)
    AbstractBridge.Inv_1 AB
    (* Invariants de glue *)
    -> Glue_1 B AB
    (* Invariants concrets *)
    -> Inv_1 B 
    (* Garde concrète *)
    -> Guard B
    (* ==> post *)
    -> AbstractBridge.CarLeave.Guard AB.
Proof.
  intros B AB AI1 Gl1 I1 Gu1.
  unfold AbstractBridge.CarLeave.Guard.
  unfold Glue_1 in *.
  rewrite <- Gl1.
  unfold total_nb_cars.
  unfold Guard in *.
  destruct Gu1.
  destruct H0.
  rewrite H.
  simpl.
  assert(Bidule : forall a b : nat, a > 0 -> a + b > 0).
  {
    intros a b.
    auto with arith.
  }
  apply Bidule.
  exact H0.
Qed.


Lemma pred_plus:
  forall n m : nat,  n > 0 -> (pred n) + m = pred (n + m).
Proof.
  intros n m.
  destruct n as [|n'].
  - (* cas n=0 *)
    intro Hcontra.
    inversion Hcontra.
  - (* cas n=S n' *)
    intro Hdummy. clear Hdummy.
    simpl.
    reflexivity.
Qed.

Theorem PO_Safety:
  forall B:State, forall AB:AbstractBridge.State,
    (* Invariants abstraits *)
    AbstractBridge.Inv_1 AB
    (* Invariants de glue *)
    -> Glue_1 B AB
    (* Invariants concrets *)
    -> Inv_1 B
    (* Garde concrète *)
    -> Guard B
    (* ==> post *)
    -> let B' := action B
       in Inv_1 B'.
Proof.
  intros B AB AI1 Gl1 I1 Gu1 B'.
  unfold B'. clear B'.
  unfold Inv_1, action.
  simpl.
  left.
  unfold Guard in *.
  destruct Gu1.
  exact H.
Qed.

Theorem PO_Simulation:
  forall (B:State), forall (AB:AbstractBridge.State),
    (* Invariants abstraits *)
    AbstractBridge.Inv_1 AB
    (* Invariants de glue *)
    -> Glue_1 B AB
    (* Invariants concrets *)
    -> Inv_1 B
    (* Garde concrète *)
    -> Guard B
    (* ==> post *)
    -> let B' := action B
       in let AB' := AbstractBridge.CarLeave.action AB
          in (* ====> Invariants de glue *)
          Glue_1 B' AB'.
Proof.
  intros A AB AI1 Gl1 I1 Gu1 B' AB'.
  unfold B', AB'. clear B' AB'.
  unfold Glue_1, action, AbstractBridge.CarLeave.action.
  simpl.
  unfold Glue_1 in *.
  rewrite <- Gl1.
  unfold total_nb_cars.
  simpl.
  rewrite plus_assoc_reverse.
  rewrite plus_comm.
  pattern (nb_cars_to_island A + nb_cars_to_mainland A + nb_cars_on_island A).
  rewrite plus_assoc_reverse.
  pattern (nb_cars_to_island A + (nb_cars_to_mainland A + nb_cars_on_island A)).
  rewrite plus_comm.
  (* pred_plus *)
  rewrite pred_plus.
  rewrite pred_plus.
  reflexivity.
  -
    unfold Guard in *.
    destruct Gu1.
    destruct H0.
    assert(Bidule : forall a b : nat, a > 0 -> a + b > 0).
    {
      intros a b.
      auto with arith.
    }
    apply Bidule.
    exact H0.
  -
    destruct Gu1.
    destruct H0.
    exact H0.
Qed.

End CarLeaveToMainland.




Theorem PO_Deadlock_Freedom:
  forall B:State, forall AB:AbstractBridge.State, 
    (* Invariants abstraits *)
    AbstractBridge.Inv_1 AB
    (* Invariants de glue *)
    -> Glue_1 B AB
    (* Invariants concrets *)
    -> Inv_1 B
    (* Guard disjunction *)
    -> CarEnterFromMainland.Guard B
       \/ CarLeaveToIsland.Guard B
       \/ CarEnterFromIsland.Guard B
       \/ CarLeaveToMainland.Guard B.
Proof.
  intros B AB HAbsInv_1 HGlue_1 HInv_1.
  unfold Inv_1 in HInv_1.
  unfold Glue_1 in HGlue_1.
  unfold CarEnterFromMainland.Guard.
  unfold CarLeaveToIsland.Guard.
  unfold CarEnterFromIsland.Guard.
  unfold CarLeaveToMainland.Guard.
  unfold AbstractBridge.Inv_1 in HAbsInv_1.
  rewrite <- HGlue_1 in HAbsInv_1.
  clear HGlue_1.
  unfold Context.max_nb_cars.
  unfold total_nb_cars in HAbsInv_1.
  destruct HInv_1 as [HInv_1 | HInv_1].
  - (* cas to_island = 0 *)
    destruct (nb_cars_on_island) as [|n].
    + (* cas 0 *)
      destruct (nb_cars_to_mainland) as [|m].
      * (* cas 0 *)
        left.
        { 
          split.
          - (* cas left *) 
            reflexivity.
          - (* cas right *)
            repeat rewrite <- plus_n_O in *.
            rewrite HInv_1.
            assert (HAxiom: AbstractBridge.Context.max_nb_cars > 0).
            { apply AbstractBridge.Context.max_nb_cars_not_zero. }
            unfold gt in HAxiom.
            exact HAxiom.
        }
      * (* cas S m *)
        right.
        right.
        right.
        SearchPattern ( S _ > 0 ).
        (* gt_Sn_O: forall n : nat, S n > 0 *)
        apply gt_Sn_O.
    + (* cas S n *)
      rewrite HInv_1 in *.
      right.
      right.
      left.
      split.
      * apply gt_Sn_O.
      * reflexivity.
  - (* cas to_mainland = 0 *)    
    (* <<< A COMPLETER >>>> *)
Qed.        

End ConcreteBridge.
