
(*  Script du Thème 01 :  fonctions récursives*)

(** Fonctions simples
let id (e:'a) : 'a = e
val id : 'a -> 'a = <fun>*)

Definition id {A:Set} (a:A) : A := a.

(*
# id 12 ;;
- : int = 12
# id true ;;
- : bool = true
*)

Eval compute in id 12.
Eval compute in id true.

Example id_12: id 12 = 12.
Proof.
compute.
reflexivity.
Qed.

Lemma id_id: 
  forall A : Set, forall a : A,
    id a = a.
Proof.
intros A a.
compute.
reflexivity.
Qed.

(**** Exercice : fonction constante
Définir en Ocaml puis traduire en Coq la fonction [constant] qui retourne toujours le premier 
de ses deux arguments.
Tester avec [constant 12 true]
Montrer que [constant (id a) b = a].*)

Definition constant {A B : Set} (a : A) (b : B) : A :=
  a.

Eval compute in constant 12 true.
Lemma constant_id : 
  forall A B : Set, forall a : A, forall b : B, 
    constant (id a) b = a.
Proof.
intros A B a b.
compute.
reflexivity.
Qed.


(*Un autre exemple de fonction.*)
Definition flip {A B C:Set} (f:A -> B -> C) : B -> A ->  C := 
  fun (b:B) (a:A) => f a b.

(* let flip (f:'a -> 'b -> 'c) : 'b -> 'a -> 'c =
   fun (b:B) (a:A) -> f a b. *)

Lemma flip_flip:
  forall A B C : Set, forall f : A -> B -> C,
    flip (flip f) = f.
Proof.
intros A B C f.
unfold flip.
reflexivity.
Qed.

Lemma flip_flip_flip_flip:
  forall A B C : Set, forall f : A -> B -> C,
    f = flip (flip (flip (flip f))).
Proof.
intros A B C f.  
rewrite flip_flip.
rewrite flip_flip.
reflexivity.
Qed.

Lemma flip_flip_flip_flip':
  forall A B C : Set, forall f : A -> B -> C,
    f = flip (flip (flip (flip f))).
Proof.
repeat rewrite flip_flip ; reflexivity.
Qed.


(*** Fonctions totales
# let rec until (pred: 'a -> bool) (f:'a -> 'a) (a:'a) : 'a =
     if pred a then until pred f (f a)
     else a ;; *)

(*

<<DECOMMENTER POUR ESSAYER>>

Fixpoint until {A:Set} (pred: A -> bool) (f:A -> A) (a:A) : A :=
  if pred a then until pred f (f a)
  else a.

*)

Fixpoint untiln {A:Set} (n:nat) (pred: A -> bool) (f:A -> A) (a:A) : A :=
  if pred a then match n with
                     | 0 => a
                     | S n' => untiln n' pred f (f a)
                 end
  else a.

Lemma untiln_id: 
  forall A : Set, forall n:nat, 
  forall pred:A -> bool, forall a : A,
    untiln n pred id a = a.
Proof.
intros A n pred a.
induction n as [|n'].
  - (* cas n=0 *)
    simpl.
    case (pred a) ; reflexivity.
  - (* cas n=S n' *)
    simpl.
    destruct (pred a).
    + (* cas (pred a) = True *)
      rewrite id_id.
      rewrite IHn'.
      reflexivity.
    + (* cas (pred a) = False *)
      reflexivity.
Qed.


(*

  * Fonctions sur les listes

*)

Require Import List.

Print list.

Check cons 1 (cons 2 (cons 3 nil)).

Example cons_infixe:
 1::2::3::nil = cons 1 (cons 2 (cons 3 nil)).
Proof.
reflexivity.
Qed.


(*** Une première fonction
let rec concat (l1 : 'a list) (l2 : 'a list) : 'a list =
  match l1 with
  | [] -> l2
  | e::l1' -> e::(concat l1' l2) *)

Fixpoint concat {A : Set} (l1 l2 : list A) : list A :=
  match l1 with
    | nil => l2
    | e::l1' => e::(concat l1' l2)
  end.

Eval compute in concat (1::2::nil) (3::4::5::nil).

Example concat_ex1:
  concat (1::2::3::nil) (4::5::nil)  = 1::2::3::4::5::nil.
Proof.
compute.
reflexivity.
Qed.

Lemma nil_concat: 
  forall A : Set, forall l : list A,
    concat nil l = l.
Proof.
intros A l.
simpl.
reflexivity.   (* ou [trivial] *)
Qed.

Lemma concat_nil_fails:
  forall A : Set, forall l : list A,
  concat l nil = l.
Proof.
intros A l.
simpl.  (* bloqué *)
Abort.

Lemma concat_nil_destruct:
  forall A : Set, forall l : list A,
  concat l nil = l.
Proof.
intros A l.
destruct l as [| e l'].
  - (* cas nil : l=nil *)
    simpl.
    reflexivity.
  - (* cas cons: l=e::l' *)
    simpl.  (* bloqué *)
Abort.

Lemma concat_nil:
  forall A : Set, forall l : list A,
  concat l nil = l.
Proof.
induction l as [|e l'].
  - (* cas de base : l=nil *)
    simpl.
    reflexivity.
  - (* cas inductif: l=e::l' *)
    simpl.
    rewrite IHl'.
    reflexivity.
Qed.

(* Le schéma de preuve qui a été
suivi : *)

Check list_ind.

(* ** Fonctions partielles
# exception Boum ;;
exception Boum
# let boum (e:'a) = raise Boum ;;
val boum : 'a -> 'b = <fun>
*)

Print option.

Definition head {A:Set} (l:list A) : option A :=
  match l with
      | nil => None
      | e::_ => Some e
  end.

Example head_ex1:
  head (1::2::3::4::nil) = Some 1.
Proof.
compute.
reflexivity.
Qed.

Lemma head_concat: 
  forall A : Set, forall l1 l2 : list A,
    head (concat l1 l2) = 
    match l1 with
      | nil => head l2 
      | e1::l1' =>  Some e1
    end.
Proof.
intros A l1 l2.
destruct l1 as [| e1'' l1''].
  - (* cas l1 = nil *)
    simpl.
    reflexivity.
  - (* cas l1 = e1''::l1'' *)
    simpl.
    reflexivity.
Qed.

(*
one-liner
*)

Lemma head_concat': 
  forall A : Set, forall l1 l2 : list A,
    head (concat l1 l2) = 
    match l1 with
      | nil => head l2 
      | e1::l1' =>  Some e1
    end.
Proof.
  destruct l1 ; trivial.
Qed. 

Definition tail {A:Set} (l:list A) : list A :=
  match l with
      | nil => nil
      | _::l' => l'
  end.

Lemma tail_amb:
  forall A : Set, forall e : A,
    tail nil = tail (e::nil).
Proof.
intros A e.
simpl.
reflexivity.
Qed.

Lemma head_tail: 
  forall A : Set, forall e : A, forall l : list A,
    head l = Some e -> e :: tail l = l.
Proof.
intros A e l Hhead.
destruct l as [| e' l'].
  - (* cas l = nil *)
    inversion Hhead. (* contradiction *)
  - (* cas l = e'::l' *)
    simpl.
    inversion Hhead as [ He ].
    reflexivity.
Qed.


(*** Exercice : [untiln] partielle
La fonction [untiln] vue précédemment devrait être réécrite pour
être une fonction partielle. En particulier, si la borne d'approximation
est atteinte, le résultat devrait être [None].

Réécrire la fonction [untiln] en conséquence. Que faire du lemme démontré
 sur cette fonction ?
*)

Fixpoint untiln2 {A:Set} (n:nat) (pred: A -> bool) (f:A -> A) (a:A) : option A :=
  if pred a then match n with
                     | 0 => None
                     | S n' => untiln2 n' pred f (f a)
                 end
  else Some a.


(*** Exercice : Renversement de liste
 *** Question 1
Définir la fonction [rev] qui renverse les élément d'une liste.
*)
Fixpoint rev {A : Set} (l : list A) : list A :=
  match l with 
    | nil => nil
    | a::b => app (rev b) (a::nil)
  end.

(**** Question 2 
Compléter l'exemple suivant :*)

Example rev_ex1: 
  rev (1::2::3::4::5::nil) = 5::4::3::2::1::nil.
Proof.
compute.
reflexivity.
Qed.

(***** Question 3
On souhaite montrer que [rev] est idempotent, mais la preuve suivante
 bloque.
*)

Theorem rev_rev_fails:
  forall A : Set, forall l : list A,
    rev (rev l) = l.
Proof.
intros A l.
induction l as [| e l'].
  - (* cas l=nil *)
    simpl.
    reflexivity.
  - (* cas l=e::l' *)
    simpl. (* bloqué *)
Abort.

(* Il nous manque une propriété importante reliant [rev], [concat] et [nil].

Démonter le lemme [rev_concat_nil] correspondant. En déduire une preuve
complète pour le théorème [rev_rev].*)
Lemma rev_concat_nil:
  forall A : Set, forall l : list A, forall e : A, rev (l ++ (e::nil)) = e::(rev l).
Proof.
intros A l e.
induction l.
-
  simpl.
  reflexivity.
-
simpl.
rewrite IHl.
reflexivity.
Qed.


Theorem rev_rev:
  forall A : Set, forall l : list A,
    rev (rev l) = l.
Proof.
intros A l.
induction l as [|e l'].
-
  unfold rev.
  reflexivity.
-
  simpl.
  rewrite rev_concat_nil.
  rewrite IHl'.
  reflexivity.
Qed.

(*** Exercice : dernier élément
 *** Question 1
Définir la fonction [last] qui retourne le dernier élément d'une
liste non-vide.  Cette fonction prendra un argument [d]  (pour défaut) que l'on retournera si la liste est vide.
*)
Fixpoint last {A : Set} (l : list A) (d : A) : A :=
  match l with
    | nil => d
    | a::l' => match l' with
                   | nil => a
                   | _::_ => last l' d
               end
  end.


(**** Question 2
Montrer le lemme suivant.
*)
Lemma last_single:
  forall A : Set, forall d e : A,
    last (e::nil) d = e.
Proof.
intros A d e.
unfold last.
reflexivity.
Qed.



(**** Question 3
Montrer par induction le lemme suivant.*)

Lemma last_concat:
  forall A : Set, forall l1 l2 : list A, forall d e : A,
    last (concat l1 (e::l2)) d = last (e::l2) d.
Proof.
intros A l1 l2 d e.
induction l1 as [|e1 l1'].
- (* cas l1=nil *)
  simpl.
  destruct l2 as [|e2 l2'] ; reflexivity.
- (* cas l1=e1::l1' *)
  simpl.
  destruct (concat l1' (e :: l2)).
  destruct l2 as [|e2' l2'].


(* a finir !!!!!!!!!!!!!!!!!!
*
*
*
*
*
*
*
*)


  simpl in IHl1'.
  destruct l2.

  unfold concat.
  trivial.
  trivial.
Qed.


(** Fonctions d'ordre supérieur
 ** Exercice : la fonction map *)

Fixpoint map {A B :Set} (f:A -> B) (l:list A) : list B :=
  match l with
    | nil => nil
    | e::l' => (f e) :: map f l'
  end.

(***** Question
Démontrer les lemmes suivants :*)


Lemma head_map:
  forall A B : Set, forall e : A, forall l : list A, forall f : A -> B,
    head (map f (e::l)) = Some (f e).
Proof.



(* <<DECOMMENTER>>
Lemma map_id:
  forall A : Set, forall l : list A,
    map id l = l.
Proof.
  <<COMPLETER>>
*)

(* <<DECOMMENTER>>
Lemma map_concat:
  forall A B : Set, forall l1 l2 : list A, forall f : A -> B,
    map f (concat l1 l2) = concat (map f l1) (map f l2).
Proof.
  <<COMPLETER>>
*)

(*

  ** Exercice : la fonction foldr

*)

Fixpoint foldr {A B :Set} (f:A -> B -> B) (u:B) (l:list A) : B :=
  match l with
    | nil => u
    | e::l' => f e (foldr f u l') 
  end.

(*

  *** Question 1

Définir une fonction [folder_id] :

(* <<REPONDRE ICI>> *)

(* telle que l'exemple suivant est prouvable : *)

(* <<DECOMMENTER>> *)
Example foldr_id_ex1:
  foldr folder_id nil (1::2::3::nil) = (1::2::3::nil).
Proof.
  <<COMPLETER ICI>>
*)

(* En déduire le théorème général suivant : *)

(* <<DECOMMENTER>>
Theorem foldr_id:
  forall A : Set, forall l : list A,
    foldr folder_id nil l = l.
Proof.
  <<COMPLETER ICI>>
*)

*)

(*

  *** Question 2

En vous inspirant de l'exemple suivant  (à compléter) :

*)

(* <<DECOMMENTER>>
Example folder_id_ex2:
 foldr folder_id (4::5::6::nil) (1::2::3::nil) = 1::2::3::4::5::6::nil.
Proof.
  <<COMPLETER ICI>>
*)

(* Définir une fonction [fold_concat] *)

(* <<REPONDRE ICI>> *)

(* telle que l'on peut démontrer : *)

(* <<DECOMMENTER>>
Theorem fold_concat:
  forall A : Set, forall l1 l2 : list A,
    foldr folder_id l2 l1 = concat l1 l2.
Proof.
  <<COMPLETER ICI>>
*)

(*

  *** Question 3

Selon les mêmes principes, redéfinir la fonction [map] en utilisant [foldr].

Tester avec l'exemple correspondant à [map (fun n : nat => S n) (1::2::3::4::5::nil)].

Démontrer le théorème général reliant [map] et [foldr].

*)

(* <<REPONDRE ICI>> *)

(*

  *** Question 4

Mêmes questions pour la fonction [rev]

*)

(*

  ** Exercice : pliage à gauche (difficile)

La fonction de pliage à gauche est la suivante :

*)

Fixpoint foldl {A B :Set} (f:A -> B -> A) (u:A) (l:list B) : A :=
  match l with
    | nil => u
    | e::l' => foldl f (f u e) l'
  end.

(*

  *** Question 1

Définir une fonction _non-récursive_ [foldl'] de pliage à gauche à partir de [foldr].

*)

(* <<REPONDRE ICI>> *)

(* Démontrer le théorème suivant : *)

(* <<DECOMMENTER>>
Theorem foldl'_foldl:
  forall A B : Set, forall f : A -> B -> A,
  forall l : list B, forall u : A,
    foldl f u l = foldl' f u l.
Proof.
  <<COMPLETER ICI>>
*)

(*

  *** Question 2

Reconstruire les fonctions [id], [concat], [rev] et [map] avec
 [foldl] et [foldl'].  

Démontrer les lemmes et théorèmes qui
semblent les plus pertinents. Alterner des preuves pour [foldl]
 et des preuves pour [foldl']   ainsi que des preuves
 exploitant le théorème [foldl'_foldl] ci-dessus.

*)

