(*
  let id (e:'a) : a = e;
*)

Definition id {A:Set} (e:A) : A := e.
(* Set est le super-type des types de données *)

Eval compute in id 12. 
Eval compute in id true.

Example id_12: id = 12.
Proof.
compute.
reflexivity.
Qed.

Lemma id_id:
  forall A : Set, forall x : A, id x = x.
Proof.
intros A x.
unfold id.
reflexivity.
Qed.

(*
let nul (a:'a) (b:'b) : 'a = a;
*)
Definition returnA {A:Set} {B:Set} (a:A) (b:B) : A := a.
Eval compute in returnA 12 true.
Eval compute in returnA false 2.

Lemma returnA_lemma1:
  forall A B : Set, forall a : A, forall b : B, returnA a b = a.
Proof.
intros A B a b.
unfold returnA.
reflexivity.
Qed.



(* 
let rec untiln (n : int) (pred : 'a -> bool) (f:'a -> 'a) (a:'a) : 'a =
  if pred a then 
    match n with 
      | 0 => a
      | _ => untiln (n-1) pred f (f a)
  else a;;
*)

Fixpoint untiln {A : Set} (n : nat) (pred : A -> bool) (f : A -> A) (a:A) : A :=
  if pred a then
    match n with
      | 0 => a
      | S n' => untiln n' pred f (f a)
    end
  else a.


(*
let rec concat (a : 'a list) (b : 'a list) : 'a list = 
  match a with
  | [] -> b
  | h::q -> h::(concat q b);;
*)
Require Import List.
Fixpoint concat {A : Type} (a : list A) (b : list A) : list A:=
  match a with
  | nil => b
  | cons h q => cons h (concat q b)
  end.


Lemma concat1 :
  forall A : Type, forall b : list A, concat nil b = b.
Proof.
intros A b.
simpl.
reflexivity.
Qed.


Lemma concat2 :
  forall A : Type, forall a : list A, concat a nil = a.
Proof.
intros A a.
induction a as [|elt l'].
(* 2 branches *)
(* Cas 1 l = nil *)
unfold concat.
reflexivity.
(* Cas 2 l = elt :: l' *)
simpl.
rewrite IHl'.
reflexivity.
Qed.
