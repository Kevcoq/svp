
(* Script Coq pour le Thème 7 : types inductifs *)

Require Import Arith.
Require Import List.

(*---------------------------------------------------------------*)
(** * Types énumérés *)

Print bool.

Inductive Couleur : Set :=
 | pique : Couleur
 | coeur : Couleur
 | carreau : Couleur
 | trefle : Couleur.

Fixpoint valeur_couleur (c:Couleur) : nat :=
  match c with
  | pique => 1
  | coeur => 2
  | carreau => 3
  | trefle => 4
  end.

Example ex_valeur_couleur1: valeur_couleur coeur = 2.
Proof.
  compute. reflexivity.
Qed.

Check Couleur_ind.

Lemma couleur_surj: 
  forall c : Couleur,
    c = pique \/ c = coeur \/ c = carreau \/ c = trefle.
Proof.
  destruct c. (* essayer aussi avec induction c *)
  - (* cas pique *)
    left.
    reflexivity.
  - (* cas coeur *)
    right.
    left.
    reflexivity.
  - (* cas carreau *)
    right. right.
    left.
    reflexivity.
  - (* cas trefle *)
    right. right. right.
    reflexivity.
Qed.

(** ** Exercice *)

Lemma borne_valeur:
  forall c : Couleur,
    (0 < (valeur_couleur c)) /\ ((valeur_couleur c) <= 4).
Proof.
  destruct c.
  - (* cas pique *)
    compute.
    auto with arith.
  - (* cas coeur *)
    compute.
    auto with arith.
  - (* cas carreau *)
    compute.
    auto with arith.
  - (* cas trefle *)
    compute.
    auto with arith.
Qed.

  
(*---------------------------------------------------------------*)
(** * Types paramétrés *)

Inductive geom : Set :=
  | point: nat -> nat -> geom
  | segment: nat -> nat -> nat -> nat -> geom
  | triangle: nat -> nat -> nat -> nat -> nat -> nat -> geom
  | nogeom : geom.

Fixpoint compose_geom (g1 g2:geom) : geom := 
  match g1 with
  | point x1 y1 => match g2 with
                   | point x2 y2 => segment x1 y1 x2 y2
                   | segment x2 y2 x3 y3 => triangle x1 y1 x2 y2 x3 y3
                   | _ => nogeom
                   end
  | segment x1 y1 x2 y2 => match g2 with
                   | point x3 y3 => triangle x1 y1 x2 y2 x3 y3
                   | _ => nogeom
                   end
  | _ => nogeom
  end.

Example ex_segment: compose_geom (point 0 0) (point 1 1) =
  segment 0 0 1 1.
Proof.
  compute. reflexivity.
Qed.

Check geom_ind.

(** ** Exercice *)

Lemma compose_nogeom: 
  forall g : geom, compose_geom nogeom g = nogeom.
Proof.
  destruct g.
  - (* cas point *)
    compute.
    reflexivity.
  - (* cas segment *)
    compute.
    reflexivity.
  - (* cas triangle *)
    compute.
    reflexivity.
  - (* cas nogeom *)
    compute.
    reflexivity.
Qed. 




(*---------------------------------------------------------------*)
(** * Types polymorphes *)

Inductive maybe (A:Set) : Type :=
  Nothing : maybe A
| Just : A -> maybe A.

Arguments Nothing [A].
Arguments Just [A] _.

Check maybe_ind.

Definition maybe_map {A B:Set} (f:A->B) : maybe A -> maybe B :=
  fun (ma:maybe A) => match ma with
                        | Nothing => Nothing
                        | Just a => Just (f a)
                      end.

Lemma maybe_map_id:
  forall A : Set, forall ma : maybe A,
    maybe_map (fun a:A => a) ma = (fun (ma:maybe A) => ma) ma.
Proof.
  intros A ma.
  unfold maybe_map.
  destruct ma as [|a] ; reflexivity.
Qed.

(** ** Exercice *)

Definition maybe_compose {A B C : Set} (f:A->B) (g:B->C) : A->C :=
  fun (a:A) => g (f a).

Lemma maybe_map_compose:
  forall A B C : Set, forall ma : maybe A,
  forall f : A->B, forall g: B->C,
    maybe_map (maybe_compose f g) ma 
    = (maybe_compose (maybe_map f) (maybe_map g)) ma.
Proof.
  intros A B C ma f g.
  unfold maybe_compose.
  unfold maybe_map.
  simpl.
  destruct ma.
  - (* cas nothing *)
    reflexivity.
  - (* cas just *)
    reflexivity.
Qed.

    

(*---------------------------------------------------------------*)
(** * Types récursifs simples *)

Check nat_ind.

Inductive list_Couleur : Set :=
  | Nil_Couleur: list_Couleur
  | Cons_Couleur: Couleur -> list_Couleur -> list_Couleur.

Check list_Couleur_ind.

(** ** Exercice *)

Fixpoint meme_couleur (c : Couleur) (l : list_Couleur) : Prop :=
  match l with
    | Nil_Couleur => True
    | Cons_Couleur e l' => (e = c) /\ meme_couleur c l'
  end.

Fixpoint somme_couleurs (l : list_Couleur) : nat :=
  match l with
    | Nil_Couleur => 0
    | Cons_Couleur e l' => (valeur_couleur e) + (somme_couleurs l')
  end.

Fixpoint longueur (l : list_Couleur) : nat :=
  match l with
    | Nil_Couleur => 0
    | Cons_Couleur _ l' => S (longueur l')
  end.

Lemma couleur_unique:
  forall c : Couleur, forall l : list_Couleur,
    meme_couleur c l
    -> (somme_couleurs l) = (longueur l) * (valeur_couleur c).
Proof.
  intros c l meme.
  induction l.
  - (*cas nil*)
    unfold longueur.
    unfold somme_couleurs.
    simpl.
    reflexivity.
  - (*cas l*)
    simpl.
    simpl in meme.
    destruct meme.
    rewrite H.
    rewrite IHl.
    reflexivity.
    exact H0.
Qed.


(*---------------------------------------------------------------*)
(** * Types récursifs polymorphes *)

Print list.

Check list_ind.


(** ** Exercice *)
(** *** Question 1 *)
Inductive bintree (A : Type) : Type :=
  leaf: bintree A
| node: A -> bintree A -> bintree A -> bintree A. 

Arguments leaf [A].
Arguments node [A] _ _ _.

(** *** Question 2 *)
(* <<définir nsize ici >> *)
Fixpoint nsize {A : Type} (a : bintree A) :=
  match a with
    | leaf => 0
    | node n g d => S ((nsize g) + (nsize d))
  end.
          
Definition bintree_ex1 : bintree nat :=
  (node 1 
        (node 2 
              (node 3 leaf leaf)
              (node 4 
                    (node 5 leaf leaf)
                    (node 6 leaf (node 7 leaf leaf))))
        (node 8
              (node 9 leaf leaf)
              leaf)).


Example nsize_ex1: 
  nsize bintree_ex1 = 9.
Proof.
  compute. reflexivity.
Qed.          

(** *** Question 3 *)

(* <<<DEFINIR lsize ICI>>> *)
Fixpoint lsize {A : Type} (a : bintree A) :=
  match a with
    | leaf => 1
    | node n g d => (lsize g) + (lsize d)
  end.

Example lsize_ex1:
  lsize bintree_ex1 = 10.
Proof.
  compute. reflexivity.
Qed.

Lemma node_leaf_size:
  forall A : Type, forall t : bintree A,
    (lsize t) = S (nsize t).
Proof.
  intros A t.
  induction t.
  - (* cas leaf *)
    simpl.
    reflexivity.
  - (* cas bintree *)
    simpl.
    rewrite IHt1.
    rewrite IHt2.
    SearchPattern((S ?X) + _ = (S (?X + _))).
    (* plus_Sn_m: forall n m : nat, S n + m = S (n + m) *)
    rewrite plus_Sn_m.
    SearchRewrite(_ + _).
    (* plus_comm: forall n m : nat, n + m = m + n *)
    rewrite plus_comm.
    rewrite plus_Sn_m.
    rewrite plus_comm.
    reflexivity.
Qed.




(** ** Exercice *)

(** *** Question 1 *)
  
(* <<DEFINIR lprefix ICI>> *)
Fixpoint lprefix {A : Type} (a : bintree A) : (list A) :=
  match a with
      leaf => nil
    | node n g d => (app (cons n (lprefix g)) (lprefix d))
  end.



Example lprefix_ex1:
  lprefix bintree_ex1 = 1 :: 2 :: 3 :: 4 :: 5 :: 6 :: 7 :: 8 :: 9 :: nil.
Proof.
  compute. reflexivity.
Qed.

(** *** Question 2 *)

Lemma nsize_length: 
  forall A : Set, forall t : bintree A,
    nsize t = length (lprefix t).
Proof.
  intros A t.
  induction t.
  - (* cas leaf *)
    simpl.
    reflexivity.
  - (* cas bintree *)
    simpl.
    rewrite IHt1.
    rewrite IHt2.
    SearchRewrite(length (_ ++ _)).
    (* app_length: forall (A : Type) (l l' : list A), length (l ++ l') = length l + length l' *)
    rewrite app_length.
    reflexivity.
Qed.



(** ** Exercice *)

(** *** Question 1 *)
  
(* <<<DEFINIR bmap ICI>>> *)
Fixpoint bmap {A B : Type} (f:A->B) (a : bintree A) :  bintree B :=
  match a with
    | leaf => leaf
    | node n g d => node (f n) (bmap f g) (bmap f d)
  end.



Example bmap_ex1:
  bmap (fun (n:nat) =>  n + n) bintree_ex1
  = node 2
         (node 4
               (node 6 leaf leaf)
               (node 8
                     (node 10 leaf leaf)
                     (node 12 leaf (node 14 leaf leaf))))
         (node 16 (node 18 leaf leaf) leaf).
Proof.
  compute. reflexivity.
Qed.

(** *** Question 2 *)

Lemma map_bmap:
  forall A B : Set, 
  forall f : A->B, forall t : bintree A,
    lprefix (bmap f t) = map f (lprefix t).
Proof.
  intros A B f t.
  induction t.
  - (* cas leaf *)
    simpl.
    reflexivity.
  - (* cas bintree *)
    simpl.
    rewrite IHt1.
    rewrite IHt2.
    SearchRewrite(map ?X _ ++ map ?X _).
    (* map_app:  forall (A B : Type) (f : A -> B) (l l' : list A),  map f (l ++ l') = map f l ++ map f l' *)
    rewrite map_app.
    reflexivity.
Qed.



(*---------------------------------------------------------------*)
(** * Récursion mutuelle *)

Inductive gentree (A:Set) : Set :=
| gnode: A -> forest A -> gentree A
with forest (A:Set) : Set :=
| fnil: forest A
| fcons: gentree A -> forest A -> forest A.

Arguments gnode [A] _ _.
Arguments fnil [A].
Arguments fcons [A] _ _.

Check gentree_ind.

Check forest_ind.

Fixpoint gsize {A:Set} (t:gentree A) : nat :=
  match t with
    | gnode _ f => S (fsize f)
  end
with fsize {A:Set} (f:forest A) : nat :=
       match f with
         | fnil => 0
         | fcons e f' => (gsize e) + (fsize f')
       end.

Fixpoint lgprefix {A:Set} (t:gentree A) : list A :=
  match t with
    | gnode e f => e::(lfprefix f)
  end
with lfprefix {A:Set} (f:forest A) : list A :=
    match f with
      | fnil => nil
      | fcons e f' => (lgprefix e) ++ (lfprefix f')
    end.

Lemma gsize_length:
  forall A : Set, forall t : gentree A,
    gsize t = length (lgprefix t).
Proof.
  intros A t.
  induction t.
  simpl.
Abort.

Scheme gentree_ind' :=
  Induction for gentree Sort Prop
  with forest_ind' :=
    Induction for forest Sort Prop.

Check gentree_ind'.

Lemma gsize_length:
  forall A : Set, forall t : gentree A,
    gsize t = length (lgprefix t).
Proof.
  intros A t.
  elim t using gentree_ind' with 
  (P0:= fun f:forest A => fsize f = length (lfprefix f)).
  - (* cas des noeuds *)
    intros a f H1.
    simpl.
    rewrite H1.
    reflexivity.
  - (* cas des forêts vides *)
    simpl.
    reflexivity.
  - (* cas des forêts non-vides *)
    intros g Hg f Hf.
    simpl.
    rewrite Hg.
    rewrite Hf.
    (* SearchRewrite (length (_ ++ _))
       app_length:
         forall (A : Type) (l l' : list A), 
             length (l ++ l') = length l + length l' *)
    rewrite app_length.
    reflexivity.
Qed.


(** ** Exercice *)
(** *** Question 1 *)
(* <<<DEFINIR gmap ICI>>> *)
Fixpoint gmap {A B : Set} (f:A->B) (a : gentree A) :  gentree B :=
  match a with
    | gnode n fo => gnode (f n) (fmap f fo) 
  end
with fmap {A B :Set} (f:A->B) (fo : forest A) : forest B :=
       match fo with
         | fnil => fnil
         | fcons e fo' => (fcons (gmap f e) (fmap f fo'))
       end.



(** *** Question 2 *)
Lemma map_gmap:
  forall A B : Set, 
  forall h : A->B, forall t : gentree A,
    lgprefix (gmap h t) = map h (lgprefix t).
Proof.
  intros A B h t.
  elim t using gentree_ind' with (P0:=fun f:forest A => lfprefix (fmap h f) = (map h (lfprefix f))).
  - (* cas des noeuds *)
    intros a f lfpref.
    simpl.
    rewrite lfpref.
    reflexivity.
  - (* cas des forets vides *)
    simpl.
    reflexivity.
  - (* cas des forets non vide *)
    intros g lgpref f lfpref.
    simpl.
    rewrite lgpref.
    rewrite lfpref.
    SearchRewrite(map ?X _).
    (* map_app:  forall (A B : Type) (f : A -> B) (l l' : list A),  map f (l ++ l') = map f l ++ map f l' *)
    rewrite map_app.
    reflexivity.
Qed.


(*---------------------------------------------------------------*)
(** * Types dépendants *)
Inductive domino : nat -> nat -> Type :=
  | block: forall m n, domino m n 
  | chain: forall m n p, domino m n -> domino n p -> domino m p.

Arguments chain [m n p] _ _.

Eval compute in (chain (chain (block 3 8) (block 8 4)) (block 4 9)).

Inductive vector (A:Set) : nat -> Set :=
  | vNil : vector A O
  | vCons : forall n, A -> vector A n -> vector A (S n).

Arguments vNil [A].
Arguments vCons [A][n] _ _.

Check vector_ind.

Fixpoint vapp {A:Set} {n1:nat} (v1:vector A n1) {n2:nat} (v2:vector A n2) : vector A (n1 + n2) :=
  match v1 in (vector _ n1) return (vector A (n1 + n2)) with
    | vNil => v2
    | vCons _ e v1' => vCons e (vapp v1' v2)
  end.

Example vapp_ex1:
  vapp (vCons 1 (vCons 2 (vCons 3 vNil)))
       (vCons 4 (vCons 5 vNil))
  = vCons 1 (vCons 2 (vCons 3 (vCons 4 (vCons 5 vNil)))).
Proof.
  compute.
  reflexivity.
Qed.

Definition vlength {A:Set} {n:nat} (v:vector A n) : nat := n.


(** ** Exercice *)
Lemma vlength_vCons:
  forall A : Set,forall a:A, forall n:nat, forall v:vector A n,
    vlength (vCons a v) = S n.
Proof.
  intros A a n v.
  unfold vlength.
  reflexivity.
Qed.


Lemma vappa_vlength:
  forall A : Set,
  forall n1 n2 : nat,
  forall v1 : vector A n1,
  forall v2 : vector A n2,
    vlength (vapp v1 v2) = n1 + n2.
Proof.
  intros A n1 n2 v1 v2.
  induction v1.
  - (* vNil *)
    simpl.
    reflexivity.
  - (* vCons *)
    simpl.
    apply vlength_vCons.
Qed.


(** ** Exercice *)
(* <<DEFINIR vmap ICI>> *)
Fixpoint vmap {A B : Set} {n : nat} (f: A -> B) (v : vector A n) : vector B n :=
  match v with
      vNil => vNil
    | vCons n a b => vCons (f a) (vmap f b)
  end.

Example vmap_ex1:
  vmap (fun b:bool => match b with
                        | true => 1
                        | false => 0
                      end) (vCons true (vCons false vNil))
  = vCons 1 (vCons 0 vNil).
Proof.
  compute. reflexivity.
Qed. 


(** ** Exercice *)

(* <<DEFINIR list_from_vect ICI>>> *)
Fixpoint list_from_vect {A : Set} {n : nat} (v : vector A n) : list A :=
  match v with
      vNil => nil
    | vCons n a b => cons a (list_from_vect b)
  end.

Example list_from_vect_ex1:
  list_from_vect (vCons true (vCons false vNil)) = true :: false :: nil.
Proof.
  compute. reflexivity.
Qed.

(** ** Exercice *)

Lemma vmap_map:
  forall A B:Set,
  forall f : A->B,
  forall n:nat,
  forall v : vector A n,
    list_from_vect (vmap f v) = map f (list_from_vect v).
Proof.
  intros A B f n v.
  induction v.
  - (* cas vNil *)
    simpl.
    reflexivity.
  - (* cas vCons *)
    simpl.
    rewrite IHv.
    reflexivity.
Qed.


(** ** Exercice *)
Fixpoint vect_from_list {A : Set} (l : list A) : vector A (length l) :=
  match l in (list _) return (vector _ (length l)) with 
      nil => vNil
    | cons h q => vCons h (vect_from_list q)
  end.

 Example vect_from_list_ex1:
  vect_from_list (true::false::nil) = vCons true (vCons false vNil).
Proof.
  compute. reflexivity.
Qed.

(** ** Exercice *)

Theorem vect_list_convert:
  forall A : Set,
  forall l : list A,
    list_from_vect (vect_from_list l) = l.
Proof.
  intros A l.
  induction l.
  - (* cas nil *)
    simpl.
    reflexivity.
  - (* cas cons *)
    simpl.
    rewrite IHl.
    reflexivity.
Qed.
            
