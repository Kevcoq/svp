
(*Script du thème 2 : les booléens*)

Require Import List.

Check True. 
Check False.

Check true.
Check false.
Check bool.

Print bool.

Definition et (a:bool) (b:bool) : bool :=
  if a then b else false.

Example et_ex1:
  et true true = true.
Proof.
compute. reflexivity.
Qed.

Lemma et_false:
  forall b : bool, et false b = false.
Proof.
intro b.
unfold et.
reflexivity.
Qed.

(*** Exercice
Définir la disjonction [ou] et démontrer un lemme comparable à [et_false] ci-dessus.*)

Definition ou (a:bool) (b:bool) : bool :=
  if a then a else b.

Lemma ou_vrai:
  forall b : bool, ou true b = true.
Proof.
intro b.
unfold ou.
reflexivity.
Qed.


(*** Exercice
 *** Question 1 :
Définir la fonction [filter] :*)


Fixpoint filter (f : bool->bool) (l : list bool) : list bool :=
  match l with
    | nil => nil
    | a::b => if (f a) then a::(filter f b) else filter f b
  end. 

Example filter_ex1:
  filter (fun a:bool => a)  (false::true::false::false::true::nil) = true::true::nil.
Proof.
compute.
reflexivity.
Qed.



(**** Question 2 
Montrer deux lemmes qui vous semblent des propriétés "limites" de
 [filter].*)

Lemma filter_nil :
  forall f : bool -> bool, filter f nil = nil.
Proof.
intro f.
unfold filter.
reflexivity.
Qed.

Lemma filter_true :
  forall l : list bool, filter (fun a:bool => true) l = l.
Proof.
intro l.
induction l.
unfold filter.
reflexivity.
simpl.
rewrite IHl.
reflexivity.
Qed.