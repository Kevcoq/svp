
(*

Script du thème 2 : arithmétique.

*)

Require Import Arith.
Require Import List.

Print nat.

Example Zero: 0 = O.
Proof. reflexivity. Qed.

Example Cinq: 5 = S (S (S (S (S O)))).
Proof. reflexivity. Qed.

Definition double (n : nat) : nat := 2 * n. 

Example square_2_is_2plus2: 
  double 2 = 2 + 2.
Proof.
compute.
reflexivity.
Qed.

Lemma double_plus:
forall n : nat, double n = n + n.
Proof.
destruct n as [| n']. (* raisonnement par cas *)
  - (* cas de base: n=0 *)
    compute.  (* remarque : terme clos *)
    reflexivity.
  - (* cas récursif: n=S n' *)
    unfold double.
    simpl.
    SearchPattern ( S _ = S _ ).
    (* eq_S: forall x y : nat, x = y -> S x = S y *)
    apply eq_S.
    SearchPattern ( ?X + 0 = ?X).
    (* plus_0_r: forall n : nat, n + 0 = n *)
    rewrite plus_0_r.
    reflexivity.
Qed.

Lemma double_plus':
forall n : nat, double n = n + n.
Proof.
intro n.
unfold double.
ring.
Qed.

Check nat_ind.

Lemma plus_0_r':
  forall n : nat, n + 0 = n.
Proof.
intro n.
induction n as [| n'].
  - (* cas n=0 *)
    trivial.
  - (* cas n = S n' *)
    simpl.
    rewrite IHn'.
    reflexivity.
Qed.

Lemma plus_0_r'':
  forall n : nat, n + 0 = n.
Proof.
  auto with arith.
Qed.

(*** Exercice
La relation naturelle entre les listes et les entiers naturels est bien sûr la notion de longueur de liste.

 *** Question 1
Définir une fonction [longueur] retournant la longueur d'une liste.*)
Fixpoint longueur {A : Set} (l : list A) : nat :=
  match l with
    | nil => 0
    | (cons h q)  => S(longueur q)
  end.

(***** Question 2 *)

Print length.

(** Montrer que les deux fonctions [longueur] et [length]
effectuent le même calcul.*)
Lemma longueur_length:
  forall A : Set, forall l : list A, (longueur l) = (length l).
Proof.
induction l.
- (* cas nil *)
  compute.
  reflexivity.
-(* cas h::q *)
  simpl.
  auto with arith.
Qed.  


(***** Question 3
Démontrer le lemme suivant :*)

Lemma length_app:
  forall A : Set, forall l1 l2 : list A,
    length (l1 ++ l2) = (length l1) + (length l2).
Proof.
  induction l1 as [|e1 l1'].
  - (* cas nil *)
    simpl.
    reflexivity.
  - (* cas h::q *)
    destruct l2 as [|e2 l2'].
    * (* cas nil *)
      simpl.
      rewrite IHl1'.
      simpl.
      reflexivity.
    * (* cas a0::l2*)
      simpl.
      rewrite IHl1'.
      simpl.
      reflexivity.
Qed.



(***** Question 4
Montrer que [length (map f l) = length l].*)
Fixpoint map {A B :Set} (f:A -> B) (l:list A) : list B :=
  match l with
    | nil => nil
    | e::l' => (f e) :: map f l'
  end.


Lemma length_map:
  forall A : Set, forall B : Set, forall l : list A, forall f : A -> B, length(map f l) = length l.
Proof.
  induction l as [|e l'].
  -(*cas nil*)
    simpl.
    reflexivity.
  -(*cas h::q*)
    simpl.
    intro f.
    rewrite IHl'.
    reflexivity.
Qed.  




(**** Exercice 
*** Question 1
Définir la fonction [sum] calculant : $\sum_{i=0}^{n} i$.
*)
Fixpoint sum (n : nat) : nat :=
  match n with
    |0 => 0
    |S(x) => n+(sum(x))
  end.



(***** Question 2  (un peu plus corsée)
Démontrer le théorème classique de la somme : $2 \times \sum^n_{i=1} = n * (n + 1)$.*)

Lemma two_times_sum_aux:
  forall k : nat, forall n : nat,
   n + S(n + 2 * k) =  n +k + S (n + k).
Proof.
  intros k n.
  ring.
Qed.
  

Lemma plus_one_S:
  forall n : nat, n + 1 = S n.
Proof.
  induction n as [|n'].
  - (* cas 0 *)
    ring.
  - (* cas S(x) *)
    simpl.
    rewrite IHn'.
    reflexivity.
Qed.



Lemma two_times_sum:
  forall n : nat, 2*(sum n) = n*(n+1).
Proof.
  induction n as [|n'].
  - (*cas 0*)
    simpl.
    reflexivity.
  -(*cas S(x) *)
    simpl.
    SearchRewrite ( _ + 0 ).
    (* plus_0_r: forall n : nat, n + 0 = n *)
    rewrite plus_0_r.
    assert (Haux: n' + sum n' + S (n' + sum n') = n' + S ( n' + 2 * sum n')).
    { 
      rewrite two_times_sum_aux.
      reflexivity.
    }
    apply eq_S.
    rewrite Haux.
    rewrite IHn'.
    ring.
Qed. 
    

(**** Exercice : la factorielle
 *** Question 1
Soit la fonction factorielle sur les entiers naturels.*)
Fixpoint fact (n:nat) : nat :=
  match n with
    | O => 1
    | S m => n * fact m
  end.

Example fact_5: fact 5 = 120.
Proof.
  compute.
  reflexivity.
Qed.

(**Définir une version [fact_it] récursive terminale de la factorielle.*)
Fixpoint fact_it (n:nat) (acc:nat) : nat :=
  match n with
    | 0 => acc
    | S n' => fact_it n' (acc*n)
  end.

Example fact_it_5: fact_it 5 1 = 120.
Proof.
  compute.
  reflexivity.
Qed.

Lemma fact_it_0:
  forall n : nat, fact_it n 0 = 0.
Proof.
  induction n.
  - (* cas 0 *)
    simpl.
    reflexivity.
  - (* cas x *)
    simpl.
    rewrite IHn.
    reflexivity.
Qed.



(***** Question 2
Démontrer le lemme suivant :*)
Lemma fact_it_lemma:
  forall n k:nat, fact_it n k = k * (fact n).
Proof.
  induction n as [| n'].
  - (*cas 0*)
    simpl.
    auto with arith.
  - (* cas S(x) *)
    destruct k.
    * (* k=0 *)
      simpl.
      apply fact_it_0.
    * (* k = x *)
      simpl.
      rewrite IHn'.
      ring.
Qed.


(***** Question 3
En déduire un théorème permettant de relier
 les deux versions de la factorielle.*)
Theorem fact_it_eq_fact:
  forall n : nat, fact_it n 1 = fact n.
Proof.
  intro n.
  assert (H: fact_it n 1 = 1 * (fact n)).
  { apply fact_it_lemma. }
  rewrite H.
  SearchRewrite ( 1 * _ ).
  (* mult_1_l: forall n : nat, 1 * n = n *)
  rewrite mult_1_l.
  reflexivity.
Qed.


(**** Exercice : Fibonacci
*** Question 1
Définir une fonction [fib] de calcul de la suite
 de Fibonacci.*)
Fixpoint fib (n : nat) : nat :=
  match n with
      | 0 => 0
      | S(x) => match x with 
                    | 0  => 1
                    | S(y)=> (fib x) + (fib y)
                end
  end.

Require Import Arith.

(***** Question 2
Démontrer par cas le lemme suivant :*)
Lemma fib_plus_2: 
  forall n:nat, fib (S (S n)) = fib n + fib (S n).
Proof.
intro n.
simpl.
destruct n as [|n'].
- simpl.
  reflexivity.
- SearchRewrite ( _ + ( _ + _ ) ).
  (* plus_assoc: forall n m p : nat, n + (m + p) = n + m + p  *)
  rewrite plus_assoc.
  SearchPattern ( ?X + ?Y = ?Y + ?X ).
  (* plus_comm: forall n m : nat, n + m = m + n *)
  assert (H: forall X : nat, X + fib n' + fib (S n') = X + fib (S n') + fib n').
  { intro X.
    ring.
  }
  rewrite H.
  reflexivity.
Qed.



(***** Question 3 :
Donner une définition [fib_it] récursive terminale de Fibonacci. *)
Fixpoint fib_it (n : nat) (moins2 : nat)  (moins1 : nat) : nat :=
  match n with 
    | 0 => moins2
    | S(x) => fib_it x moins1 (moins1 + moins2)
  end.

Eval compute in fib 7.

Eval compute in fib_it 0 (fib 11)  (fib 12).



Eval compute in ((fib 7) +11)* 10.
(* 5 - 13  / 5 - 21*)

Lemma plus_S : 
  forall n m : nat,
    S (n + m) = (S n) + m.
Proof.
  auto with arith.
Qed.

Lemma fib_it_g:
 forall n:nat, forall k:nat, fib_it n (fib k) (fib (S k)) = fib (n+k).
Proof.
  induction n as [|n'].
  - simpl.
    reflexivity.
  - intro k.
    destruct n' as [|n''].
    + simpl.
      reflexivity.
    + SearchRewrite ( (S _) + _ ).
      (* plus_Sn_m: forall n m : nat, S n + m = S (n + m) *)
      assert (Heq: (S (S n'') + k) = S (S (n'' + k))).
      { ring. }
      rewrite Heq. clear Heq.
      rewrite fib_plus_2.
      SearchRewrite ( S (_ + _) ).
      rewrite plus_S.
      rewrite <- IHn'.

      simpl.
      

      destruct n''.
      simpl.
      reflexivity.
      simpl.
      reflexivity.
    simpl.
    destruct n'.
    + simpl.
      reflexivity.
    + rewrite <- IHn'.
      simpl.
      
    rewrite <- IHn.
    simpl.


(***** Question 4
Démontrer le théorème suivant :*)  
Theorem fib_it_fib:
  forall n:nat, fib_it n 1 1 = fib n.
Proof.
induction n as [|n'].
- simpl.
  reflexivity.
- destruct n'.
  * simpl.
    reflexivity.
  * rewrite fib_plus_2.
    rewrite <- IHn'.
    