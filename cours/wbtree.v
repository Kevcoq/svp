Require Import Arith.
Require Import NPeano.

(* -- WBtree *)

(* -- Extra arith *)
Module XArith.

Theorem max_refl : forall (x : nat), (max x x) = x.
induction x.
   trivial.
   simpl. rewrite IHx. trivial.
Qed.

Theorem max_com : forall (x y : nat), (max x y) = (max y x).
induction x.
  destruct y; auto.
  destruct y.
    auto.
    simpl. auto with arith.
Qed.

Theorem max_alt : forall (x y : nat), (max x y) = x \/ (max x y) = y.
induction x.
  intro. right. auto.
  destruct y.
    auto.
    simpl. elim IHx with (y:=y).
      intro. rewrite H. tauto.   
      intro. rewrite H. tauto.
Qed.

Theorem le_max : forall (x y : nat), (le x y) -> (max x y)=y.
induction x.
  auto. 
  destruct y.
    intro. elim le_Sn_0 with (n:=x). assumption.
    intro. simpl. rewrite IHx.
      trivial.
      apply le_S_n. assumption.
Qed.

Theorem max_le : forall (x y :nat), (max x y)=x -> (le y x).
induction x.
  intro. simpl. intro. rewrite H. auto with arith.
  destruct y.
    auto with arith.
    simpl. intro. apply le_n_S. apply IHx. auto with arith.
Qed.    

Theorem max_max_S : forall (x y : nat), (max x y)=x -> (max (S x) y) = (S x).
intros. rewrite max_com. apply le_max. apply le_trans with (m:=x).
  apply max_le. assumption. 
  apply le_n_Sn.
Qed.

Theorem max_n_Sn : forall (x : nat), (max x (S x)) = (S x).
intro. apply le_max. apply le_n_Sn.
Qed.

Theorem max_S_alt : forall (x y : nat), 
  (max x y) = y -> (max (S x) y) = y \/ (max (S x) y) = (S y).
intros. assert (le x y).
  apply max_le. rewrite max_com. assumption.
  elim le_lt_or_eq with (n:=x) (m:=y).
    intro. left. apply le_max. auto with arith.
    intro. right. rewrite H1. rewrite max_com. apply max_n_Sn.
    assumption.
Qed.

Axiom min_refl : forall (x : nat), (min x x) = x.
Axiom min_com : forall (x y : nat), (min x y) = (min y x).
Axiom min_alt : forall (x y : nat), (min x y) = x \/ (min x y) = y.
Axiom min_n_Sn : forall (n : nat), (min n (S n)) = n.
Axiom min_min_S : forall (x y : nat), (min x y)=y -> (min (S x) y) = y.
Axiom min_S_alt : forall (x y : nat),
 (min x y) = x -> (min (S x) y) = (S x) \/ (min (S x) y) = x.

Axiom le_min_max : forall (x y : nat), (le (min x y) (max x y)).

End XArith.


(* -- Abstraction *)
Module ABS.

Inductive btree : Set :=
  Empty
| Node : btree -> btree -> btree.

Fixpoint grow (bt:btree) : btree :=
  match bt with
      Empty => (Node Empty Empty)
    | (Node bt1 bt2) => (Node bt2 (grow bt1))
  end.

Inductive well_grown : btree -> Prop :=
   well_grown_empty : (well_grown Empty)
 | well_grown_sym : forall (bt:btree), 
                    (well_grown bt) -> (well_grown (Node bt bt))
 | well_grown_grow : forall (bt:btree), 
                    (well_grown bt) -> (well_grown (Node bt (grow bt))).

Lemma well_grown_closure : forall (bt:btree),
  (well_grown bt) -> (well_grown (grow bt)).
intros bt Hwell_grown. induction Hwell_grown.

  simpl. apply well_grown_sym. apply well_grown_empty.

  simpl. apply well_grown_grow. assumption.

  simpl. apply well_grown_sym. assumption.

Qed.

Fixpoint hmax (bt:btree) : nat :=
  match bt with
      Empty => 0
    | (Node bt1 bt2) => (S (max (hmax bt1) (hmax bt2)))
  end.

Fixpoint hmin (bt:btree) : nat :=
  match bt with
      Empty => 0
    | (Node bt1 bt2) => (S (min (hmin bt1) (hmin bt2)))
  end.

Definition wb_btree (bt:btree) : Prop :=
  (hmax bt) = (hmin bt) \/ (hmax bt) = (S (hmin bt)).

Lemma hmax_grow : forall (bt:btree),
  (hmax (grow bt)) = (hmax bt) \/ (hmax (grow bt)) = (S (hmax bt)).
induction bt.

  right. auto with arith.

  elim IHbt1.

    intro. simpl. rewrite H. left. rewrite XArith.max_com. reflexivity.

    intro. simpl. rewrite H. elim (XArith.max_alt (hmax bt1) (hmax bt2)).

      intro. rewrite H0. rewrite XArith.max_com.
      right. rewrite XArith.max_max_S. 
        reflexivity.
        assumption.

      intro. rewrite H0. rewrite XArith.max_com. 
      elim XArith.max_S_alt with (x:=(hmax bt1)) (y:=(hmax bt2)).
        intro. rewrite H1. tauto.
        intro. rewrite H1. tauto.
        assumption.

Qed.

Lemma max_hmax_grow : forall (bt:btree),
  (max (hmax bt) (hmax (grow bt))) = (hmax (grow bt)).
intro. elim hmax_grow with (bt:=bt).
  intro. rewrite <- H. rewrite XArith.max_refl. trivial.
  intro. rewrite H. apply XArith.max_n_Sn.
Qed.

Lemma hmin_grow : forall (bt:btree),
  (hmin (grow bt)) = (hmin bt)
  \/ (hmin (grow bt)) = (S (hmin bt)).
induction bt.

  right. simpl. trivial.

  simpl. elim IHbt1.

    intro. rewrite H. left. rewrite XArith.min_com. trivial.

    intro. rewrite H. elim (XArith.min_alt (hmin bt1) (hmin bt2)).

      intro. rewrite H0. 
      elim XArith.min_S_alt with (x:=(hmin bt1)) (y:=(hmin bt2)).
        intro. rewrite XArith.min_com. rewrite H1. right. auto.
        intro. rewrite XArith.min_com. rewrite H1. left. auto.
        assumption.

      intro. rewrite XArith.min_com. rewrite XArith.min_min_S.
        rewrite H0. left. auto.
        assumption.

Qed.
        
Lemma min_hmin_grow : forall (bt : btree),
  (min (hmin bt) (hmin (grow bt))) = (hmin bt).
intro. elim hmin_grow with (bt:=bt).
  intro. rewrite H. rewrite XArith.min_refl. trivial.
  intro. rewrite H. rewrite XArith.min_n_Sn. trivial.
Qed.

Lemma hmax_well_grown_S : forall (bt:btree),
  (well_grown bt) -> (hmax (grow bt)) = (S (hmax bt))
    -> (hmax bt) = (hmin bt).
intros bt H. induction H. 

  auto.

  simpl. rewrite max_hmax_grow. rewrite XArith.max_refl. rewrite XArith.min_refl.
  intro. rewrite IHwell_grown. 
    trivial.
    auto with arith.

  simpl. rewrite XArith.max_refl. rewrite max_hmax_grow. 
  intro. absurd ((hmax (grow bt)) = (S (hmax (grow bt)))).
    apply n_Sn.
    auto with arith.

Qed.

Theorem well_grown_wb : forall (bt:btree),
  (well_grown bt) -> (wb_btree bt).
intros. induction H.

   unfold wb_btree. tauto.

   unfold wb_btree. simpl. rewrite XArith.max_refl. rewrite XArith.min_refl.
   elim IHwell_grown.
     intro. left. rewrite H0. reflexivity.
     intro. right. rewrite H0. reflexivity.
  
   unfold wb_btree. simpl. rewrite max_hmax_grow. rewrite min_hmin_grow.
   elim hmax_grow with (bt:=bt).

     intro. rewrite H0. auto. elim IHwell_grown.
       intro. rewrite H1. left. trivial.
       intro. rewrite H1. right. trivial.

     intro. rewrite H0. elim IHwell_grown.
       intro. rewrite H1. right. trivial.
       intro. absurd (hmax bt = S (hmin bt)).
         rewrite hmax_well_grown_S.
           apply n_Sn.
           assumption.
           assumption.
         assumption.

Qed.

End ABS.


(* -- WBtree *)
Variable A:Set.
Variable ltb : A -> A -> bool.

Inductive btree (A:Set) : Set :=
  Empty : (btree A)
| Node : (btree A) -> A -> (btree A) -> (btree A).

Arguments Empty [A].
Arguments Node [A] _ _ _.

Fixpoint ins_heap (x:A) (bt : (btree A)) :=
  match bt with
    Empty => (Node Empty x Empty)
  | (Node bt1 y bt2) =>
      if (ltb x y) then
      	(Node bt2 x (ins_heap y bt1))
      else
      	(Node bt2 y (ins_heap x bt1))
  end.

Fixpoint heap_of (xs:(list A)) :=
  match xs with
      nil => Empty
    | (cons x xs) => (ins_heap x (heap_of xs))
  end.

Fixpoint hmax (bt:(btree A)) :=
  match bt with
      Empty => 0
    | (Node bt1 x bt2) => (S (max (hmax bt1) (hmax bt2)))
  end.

Fixpoint hmin (bt:(btree A)) :=
  match bt with
      Empty => 0
    | (Node bt1 x bt2) => (S (min (hmin bt1) (hmin bt2)))
  end.

Definition wb_btree (bt:(btree A)) : Prop :=
  (hmax bt) = (hmin bt) \/ (hmax bt) = (S (hmin bt)).

(* -- Abstraction *)
Fixpoint abs_of (bt : (btree A)) :=
  match bt with
      Empty => ABS.Empty
    | (Node bt1 _ bt2) => (ABS.Node (abs_of bt1) (abs_of bt2))
  end.

Lemma hmax_abs_hmax : forall (bt : (btree A)), 
  (hmax bt) = (ABS.hmax (abs_of bt)).
induction bt.
  trivial.
  simpl. rewrite IHbt1. rewrite IHbt2. trivial.
Qed.

Lemma hmin_abs_hmin : forall (bt : (btree A)), 
  (hmin bt) = (ABS.hmin (abs_of bt)).
induction bt.
  trivial.
  simpl. rewrite IHbt1. rewrite IHbt2. trivial.
Qed.

Lemma wb_btree_abs_wb : forall (bt : (btree A)),
  (wb_btree bt) <-> (ABS.wb_btree (abs_of bt)).
unfold wb_btree. unfold ABS.wb_btree. 
intro. rewrite hmax_abs_hmax. rewrite hmin_abs_hmin. tauto.
Qed.

Lemma abs_of_ins : forall (bt:(btree A)) (x:A),
  (abs_of (ins_heap x bt)) = (ABS.grow (abs_of bt)).
induction bt.
  trivial.  
  intros. simpl. case (ltb x a).
    simpl. rewrite IHbt1. trivial.
    simpl. rewrite IHbt1. trivial.
Qed.

Lemma hmax_ins_hmax_grow : forall (bt : (btree A)) (x:A),
  (hmax (ins_heap x bt)) = (ABS.hmax (ABS.grow (abs_of bt))).
induction bt.

  trivial.

  intros. simpl. case (ltb x a);
    simpl. rewrite hmax_abs_hmax. rewrite IHbt1. trivial.
    simpl. rewrite hmax_abs_hmax. rewrite IHbt1. trivial.
Qed.

Lemma hmin_ins_hmin_grow : forall (bt : (btree A)) (x:A),
  (hmin (ins_heap x bt)) = (ABS.hmin (ABS.grow (abs_of bt))).
induction bt.

  trivial.

  intros. simpl. case (ltb x a);
    simpl. rewrite hmin_abs_hmin. rewrite IHbt1. trivial.
    simpl. rewrite hmin_abs_hmin. rewrite IHbt1. trivial.
Qed.

Lemma grow_heap : forall (xs:(list A)), ABS.well_grown (abs_of (heap_of xs)).
induction xs.
  simpl. apply ABS.well_grown_empty.
  simpl. rewrite abs_of_ins. apply ABS.well_grown_closure. assumption.
Qed.  

Theorem wb_heap_of : forall (xs:(list A)), (wb_btree (heap_of xs)).
intro. elim wb_btree_abs_wb with (bt:=(heap_of xs)).
intros. apply H0. apply ABS.well_grown_wb.
apply grow_heap.
Qed.

