
(** Exemple du cours : Machine Feux *)

Require Import Arith.

Module Feux.

Inductive couleur : Set := Vert : couleur | Orange : couleur | Rouge : couleur.
Definition next(c:couleur) : couleur :=
  match c with
    | Vert => Orange
    | Orange => Rouge
    | Rouge => Vert
  end.

(* Contexte : constantes et axiomes *)

Module Context.
  Axiom difVR : Vert <> Rouge.
  Axiom difVO : Vert <> Orange.
  Axiom difOR : Orange <> Rouge.
End Context.



(* Etat de la machine *)
Record State : Set := 
  mkState {
      feu1 : couleur;
      feu2 : couleur;
      (* vrai : fctionne | false : HS *)
      enService : bool               
}.    




(* Invariants *)
Definition Inv_2 (B:State) : Prop :=
  B.(enService) = true \/ B.(enService) = false.

Definition Inv_3 (B:State) : Prop :=
  B.(enService) = false -> B.(feu1) = Orange /\ B.(feu2) = Orange.

Definition Inv_5 (B:State) : Prop :=
  B.(enService) = true -> B.(feu1) = Rouge \/ B.(feu2) = Rouge.

Definition Inv_6 (B:State) : Prop :=
  B.(enService) = true -> B.(feu1) <> Rouge \/ B.(feu2) <> Rouge.



Module Init.

Definition action : State :=
  mkState Orange Orange false.

Lemma PO_Safety_Inv_2:
  let B := action in Inv_2 B.
Proof.
  intro B. 
  unfold Inv_2.
  simpl.
  right.
  reflexivity.
Qed.

Lemma PO_Safety_Inv_3:
  let B := action in Inv_3 B.
Proof.
  intro B.
  unfold Inv_3.
  simpl.
  split.
  - reflexivity.
  - reflexivity.
Qed.

Lemma PO_Safety_Inv_5:
  let B := action in Inv_5 B.
Proof.
  intro B.
  unfold Inv_5.
  simpl.
  discriminate.
Qed.

Lemma PO_Safety_Inv_6:
  let B := action in Inv_6 B.
Proof.
  intro B.
  unfold Inv_6.
  simpl.
  discriminate.
Qed.

Lemma PO_Safety:
  let B := action in Inv_2 B /\ Inv_3 B /\ Inv_5 B /\Inv_6 B.
Proof.
  intro B.
  split.
  - (* cas Inv_2 *)
    apply PO_Safety_Inv_2 ; assumption. 
  - split.
    * (* cas Inv_3 *)
      apply PO_Safety_Inv_3 ; assumption. 
    * split. 
    + (* cas Inv_5 *)
      apply PO_Safety_Inv_5 ; assumption. 
    + (* cas Inv_6 *)
      apply PO_Safety_Inv_6 ; assumption. 
Qed.

End Init.











(** Evénement : FeuxEnService *)
Module FeuxEnService.
Definition Guard (B:State) : Prop :=
  B.(enService) = false.

Definition action (B:State) : State :=
  mkState Vert Rouge true.

Lemma PO_Safety_Inv_2:
  forall (B:State),
    (* Invariants *)
    Inv_2 B
    -> Inv_3 B
    -> Inv_5 B
    -> Inv_6 B
    (* Guard *)
    -> Guard B
    (* ==> post *)
    ->let B' := action B in Inv_2 B'.
Proof.
  intros B H2 H3 H5 H6 HG B'.
  unfold B'. clear B'.
  unfold action.
  unfold Inv_2.
  left.
  simpl.
  reflexivity.
Qed.


Lemma PO_Safety_Inv_3:
  forall (B:State),
    (* Invariants *)
    Inv_2 B
    -> Inv_3 B
    -> Inv_5 B
    -> Inv_6 B
    (* Guard *)
    -> Guard B
    (* ==> post *)
    ->let B' := action B in Inv_3 B'.
Proof.
  intros B H2 H3 H5 H6 HG B'.
  unfold B'. clear B'.
  unfold action.
  unfold Inv_3.
  simpl.
  discriminate.
Qed.


Lemma PO_Safety_Inv_5:
  forall (B:State),
    (* Invariants *)
    Inv_2 B
    -> Inv_3 B
    -> Inv_5 B
    -> Inv_6 B
    (* Guard *)
    -> Guard B
    (* ==> post *)
    ->let B' := action B in Inv_5 B'.
Proof.
  intros B H2 H3 H5 H6 HG B'.
  unfold B'. clear B'.
  unfold action.
  unfold Inv_5.
  simpl.
  right.
  reflexivity.
Qed.


Lemma PO_Safety_Inv_6:
  forall (B:State),
    (* Invariants *)
    Inv_2 B
    -> Inv_3 B
    -> Inv_5 B
    -> Inv_6 B
    (* Guard *)
    -> Guard B
    (* ==> post *)
    ->let B' := action B in Inv_6 B'.
Proof.
  intros B H2 H3 H5 H6 HG B'.
  unfold B'. clear B'.
  unfold action.
  unfold Inv_6.
  simpl.
  left.
  apply Context.difVR.
Qed.


Theorem PO_Safety:
  forall(B:State),
    (* Invariants *)
    Inv_2 B
    -> Inv_3 B
    -> Inv_5 B
    -> Inv_6 B
    (* Guard *)
    -> Guard B
    (* ==> post *)   
    -> let B' := action B in Inv_2 B' /\ Inv_3 B' /\ Inv_5 B' /\Inv_6 B'.
Proof.
  intros B H2 H3 H5 H6 HG B'.
  split.
  - (* cas Inv_2 *)
    apply PO_Safety_Inv_2 ; assumption. 
  - split.
    * (* cas Inv_3 *)
      apply PO_Safety_Inv_3 ; assumption. 
    * split. 
    + (* cas Inv_5 *)
      apply PO_Safety_Inv_5 ; assumption. 
    + (* cas Inv_6 *)
      apply PO_Safety_Inv_6 ; assumption. 
Qed.

End FeuxEnService.






(** Evénement : FeuxHS *)
Module FeuxHS.
Definition Guard (B:State) : Prop :=
  B.(enService) = true.

Definition action (B:State) : State :=
  mkState Orange Orange false.

Lemma PO_Safety_Inv_2:
  forall (B:State),
    (* Invariants *)
    Inv_2 B
    -> Inv_3 B
    -> Inv_5 B
    -> Inv_6 B
    (* Guard *)
    -> Guard B
    (* ==> post *)
    ->let B' := action B in Inv_2 B'.
Proof.
  intros B H2 H3 H5 H6 HG B'.
  unfold B'. clear B'.
  unfold action.
  unfold Inv_2.
  right.
  simpl.
  reflexivity.
Qed.


Lemma PO_Safety_Inv_3:
  forall (B:State),
    (* Invariants *)
    Inv_2 B
    -> Inv_3 B
    -> Inv_5 B
    -> Inv_6 B
    (* Guard *)
    -> Guard B
    (* ==> post *)
    ->let B' := action B in Inv_3 B'.
Proof.
  intros B H2 H3 H5 H6 HG B'.
  unfold B'. clear B'.
  unfold action.
  unfold Inv_3.
  simpl.
  split.
  - reflexivity.
  - reflexivity.
Qed.


Lemma PO_Safety_Inv_5:
  forall (B:State),
    (* Invariants *)
    Inv_2 B
    -> Inv_3 B
    -> Inv_5 B
    -> Inv_6 B
    (* Guard *)
    -> Guard B
    (* ==> post *)
    ->let B' := action B in Inv_5 B'.
Proof.
  intros B H2 H3 H5 H6 HG B'.
  unfold B'. clear B'.
  unfold action.
  unfold Inv_5.
  simpl.
  discriminate.
Qed.


Lemma PO_Safety_Inv_6:
  forall (B:State),
    (* Invariants *)
    Inv_2 B
    -> Inv_3 B
    -> Inv_5 B
    -> Inv_6 B
    (* Guard *)
    -> Guard B
    (* ==> post *)
    ->let B' := action B in Inv_6 B'.
Proof.
  intros B H2 H3 H5 H6 HG B'.
  unfold B'. clear B'.
  unfold action.
  unfold Inv_6.
  simpl.
  discriminate.
Qed.


Theorem PO_Safety:
  forall(B:State),
    (* Invariants *)
    Inv_2 B
    -> Inv_3 B
    -> Inv_5 B
    -> Inv_6 B
    (* Guard *)
    -> Guard B
    (* ==> post *)   
    -> let B' := action B in Inv_2 B' /\ Inv_3 B' /\ Inv_5 B' /\Inv_6 B'.
Proof.
  intros B H2 H3 H5 H6 HG B'.
  split.
  - (* cas Inv_2 *)
    apply PO_Safety_Inv_2 ; assumption. 
  - split.
    * (* cas Inv_3 *)
      apply PO_Safety_Inv_3 ; assumption. 
    * split. 
    + (* cas Inv_5 *)
      apply PO_Safety_Inv_5 ; assumption. 
    + (* cas Inv_6 *)
      apply PO_Safety_Inv_6 ; assumption. 
Qed.

End FeuxHS.








(** Evénement : changeCouleur *)
Module ChangeCouleur.

Definition Guard (B:State) : Prop :=
  B.(enService) = true.

Definition action (B:State) : State :=
  match B.(feu1), B.(feu2) with
    | Vert, x => mkState (next Vert) x true
    | Orange, x => mkState (next Orange) (next x) true
    | x, Vert => mkState x (next Vert) true
    | x, Orange => mkState (next x)  (next Orange) true
    |Rouge, Rouge => mkState Orange Orange false
    end.


Lemma PO_Safety_Inv_2:
  forall (B:State),
    (* Invariants *)
    Inv_2 B
    -> Inv_3 B
    -> Inv_5 B
    -> Inv_6 B
    (* Guard *)
    -> Guard B
    (* ==> post *)
    ->let B' := action B in Inv_2 B'.
Proof.
  intros B H2 H3 H5 H6 HG B'.
  unfold B'. clear B'.
  unfold action.
  unfold Inv_2.
  induction (feu1 B).
  - simpl.
    left.
    reflexivity.
  - 
    simpl.
    left.
    reflexivity.
  -
    destruct (feu2 B).
    +
      simpl.
      left.
      reflexivity.
    +
      simpl.
      left.
      reflexivity.
    +
      simpl.
      right.
      reflexivity.
Qed.


Lemma PO_Safety_Inv_3:
  forall (B:State),
    (* Invariants *)
    Inv_2 B
    -> Inv_3 B
    -> Inv_5 B
    -> Inv_6 B
    (* Guard *)
    -> Guard B
    (* ==> post *)
    ->let B' := action B in Inv_3 B'.
Proof.
  intros B H2 H3 H5 H6 HG B'.
  unfold B'. clear B'.
  unfold action.
  unfold Inv_3.
  simpl.
  split.
  - 
    destruct feu1 .
    +
      simpl.
      reflexivity.
    +
      simpl.
      discriminate.
    +
      destruct feu2.
      *
        simpl.
        discriminate.
      *
        simpl.
        discriminate.
      *
        simpl.
        reflexivity.
  -
    destruct feu2.
    +
      destruct feu1.
      *
        simpl.
        discriminate.
      *
        simpl.
        reflexivity.
      *
        simpl.
        reflexivity.
    +
      destruct feu1.
      *
        simpl.
        reflexivity.
      *
        simpl.
        discriminate.
      *
        simpl.
        discriminate.
    +
      destruct feu1.
      *
        simpl.
        discriminate.
      *
        simpl.
        discriminate.
      *
        simpl.
        reflexivity.      
Qed.


Lemma PO_Safety_Inv_5:
  forall (B:State),
    (* Invariants *)
    Inv_2 B
    -> Inv_3 B
    -> Inv_5 B
    -> Inv_6 B
    (* Guard *)
    -> Guard B
    (* ==> post *)
    ->let B' := action B in Inv_5 B'.
Proof.
  intros B H2 H3 H5 H6 HG B'.
  unfold B'. clear B'.
  unfold action.
  unfold Guard in *.
  unfold Inv_5 in *.
  unfold Inv_6 in *.
  unfold Inv_3 in *.
  unfold Inv_2 in *.
  case_eq (feu1 B).
  - (* cas orange *)
    intro F1V.
    simpl.
    elim H5.
    + intro H1R.
      rewrite F1V in H1R.
      discriminate H1R.
    + tauto.
    + exact HG.
  - (* cas rouge *)
    intro F1O.
    simpl.
    left.
    reflexivity.
  - (* cas rouge 2 *)
    intro F1R.
    simpl.
    case_eq (feu2 B).
    + (* cas f2 : Orange *)
      intro F2V.
      simpl.
      left.
      reflexivity.
    + (* cas f2 : Rouge *)
      intro F2O.
      simpl.
      right.
      reflexivity.
    + (* cas f2 : Orange *)
      intro F2R.
      simpl.
      discriminate.
Qed.



Lemma PO_Safety_Inv_6:
  forall (B:State),
    (* Invariants *)
    Inv_2 B
    -> Inv_3 B
    -> Inv_5 B
    -> Inv_6 B
    (* Guard *)
    -> Guard B
    (* ==> post *)
    ->let B' := action B in Inv_6 B'.
Proof.
  intros B H2 H3 H5 H6 HG B'.
  unfold B'. clear B'.
  unfold action.
  unfold Guard in *.
  unfold Inv_5 in *.
  unfold Inv_6 in *.
  unfold Inv_3 in *.
  unfold Inv_2 in *.
  case_eq (feu1 B).
  + (* f1 : vert *)
    intro f1v.
    simpl.
    left.
    discriminate.
  + (* f1 : orange *) 
    intro f1o.
    simpl.
    right.
    elim H5.
    * (* f1 : rouge *)
      intro f1r.
      rewrite f1r in f1o.
      discriminate.
    * (* f2 : rouge *)
      intro f2r.
      rewrite f2r.
      unfold next.
      discriminate.
    * (* enService *)
      exact HG.
  + (* f1 : rouge *)
    intro f1r.
    case_eq (feu2 B).
    * (* f2 v *)
      intro f2v.
      simpl.
      right.
      discriminate.
    * (* f2 o *)
      intro f2o.
      simpl.
      left.
      discriminate.
    * (* f2 r *)
      intro f2r.
      simpl.
      left.
      discriminate.
Qed.



Theorem PO_Safety:
  forall(B:State),
    (* Invariants *)
    Inv_2 B
    -> Inv_3 B
    -> Inv_5 B
    -> Inv_6 B
    (* Guard *)
    -> Guard B
    (* ==> post *)   
    -> let B' := action B in Inv_2 B' /\ Inv_3 B' /\ Inv_5 B' /\Inv_6 B'.
Proof.
  intros B H2 H3 H5 H6 HG B'.
  split.
  - (* cas Inv_2 *)
    apply PO_Safety_Inv_2 ; assumption. 
  - split.
    * (* cas Inv_3 *)
      apply PO_Safety_Inv_3 ; assumption. 
    * split. 
    + (* cas Inv_5 *)
      apply PO_Safety_Inv_5 ; assumption. 
    + (* cas Inv_6 *)
      apply PO_Safety_Inv_6 ; assumption. 
Qed.

End ChangeCouleur.

End Feux.