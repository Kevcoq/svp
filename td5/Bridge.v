
(** Exemple du cours : Machine Bridge *)

Require Import Arith.

Module Bridge.



(* Contexte : constantes et axiomes *)
Module Context.

(* Constante : nombre maximum de voitures *)
Parameter max_nb_cars : nat.

Axiom max_nb_cars_not_zero : max_nb_cars > O.

End Context.





(* Etat de la machine *)
Record State : Set := mkState {
  (* nombre de voitures vers l'île *)
  nb_cars_to_island: nat;
  (* nombre de voitures vers le continent *)
  nb_cars_to_mainland: nat;
  (* nombre de voitures sur l'île *)
  nb_cars_on_island: nat
}.





(* définition annexe : nombre total de voitures *)
Definition total_nb_cars (B:State) : nat :=
  B.(nb_cars_to_island)
  + B.(nb_cars_to_mainland)
  + B.(nb_cars_on_island).

(* Invariants *)

Definition Inv_1 (B:State) : Prop :=
 total_nb_cars  B <= Context.max_nb_cars.

Definition Inv_2 (B:State) : Prop :=
  B.(nb_cars_to_island) = 0
  \/ B.(nb_cars_to_mainland) = 0.





(** Evénement : Init *)
Module Init.

Definition Guard (limit:nat) : Prop :=
  Context.max_nb_cars = limit.

Definition action (limit:nat) : State :=
  mkState 0 0 0.

Lemma PO_Safety_Inv_1:
  forall lim : nat,
    (* Guard *)
    Guard lim
    (* ==> post *)
    -> let B := action lim
       in Inv_1 B.
Proof.
  intros lim HGuard B.
  unfold B.
  clear B.
  unfold Inv_1 , action.
  unfold total_nb_cars.
  simpl.
  SearchPattern ( 0 <= _ ).
  (* le_0_n: forall n : nat, 0 <= n *)
  apply le_0_n.
Qed.

Lemma PO_Safety_Inv_2:
  forall lim : nat,
    (* Guard *)
    Guard lim
    (* ==> post *)
    -> let B := action lim
       in Inv_2 B.
Proof.
  intros lim HGuard B.
  unfold B. clear B.
  unfold Inv_2, action.
  simpl.
  left.
  reflexivity.
Qed.

Lemma PO_Safety:
  forall lim : nat,
    (* Guard *)
    Guard lim
    (* ==> post *)
    -> let B := action lim
       in Inv_1 B /\ Inv_2 B.
Proof.
  intros lim HGuard B.
  split.
  - (* cas Inv_1 *)
    apply PO_Safety_Inv_1 ; assumption.
  - (* cas Inv_2 *)
    apply PO_Safety_Inv_2 ; assumption.
Qed.

End Init.






(** Evénement : CarEnterFromMainland *)
Module CarEnterFromMainland.

Definition Guard (B:State) : Prop :=
  B.(nb_cars_to_mainland) = 0 
  /\ B.(nb_cars_to_island) + B.(nb_cars_on_island) < Context.max_nb_cars.

Definition action (B:State) : State :=
  mkState (S B.(nb_cars_to_island))
           B.(nb_cars_to_mainland)
               B.(nb_cars_on_island).

Lemma PO_Safety_Inv_1:
  forall (B:State),
    (* Invariants *)
    Inv_1 B
    -> Inv_2 B
    (* Guard *)
    -> Guard B
    (* ==> post *)
    -> let B' := action B
       in Inv_1 B'.
Proof.
  intros B HInv1 HInv2 HGuard B'.
  unfold B'. clear B'.
  unfold action.
  unfold Inv_1.
  unfold total_nb_cars.
  simpl.
  SearchPattern ( S _ <= _).
  (* lt_le_S: forall n m : nat, n < m -> S n <= m *)
  apply lt_le_S.
  unfold Inv_1 in *.
  unfold Inv_2 in *.
  unfold Guard in *.
  destruct HGuard as [HGuard1 HGuard2].
  rewrite HGuard1.
  rewrite <- plus_assoc.
  SearchRewrite (0 + _).
  (* plus_O_n: forall n : nat, 0 + n = n *)
  rewrite plus_O_n.
  exact HGuard2.
Qed.

Lemma PO_Safety_Inv_2:
  forall (B:State),
    (* Invariants *)
    Inv_1 B
    -> Inv_2 B
    (* Guard *)
    -> Guard B
    (* ==> post *)
    -> let B' := action B
       in Inv_2 B'.
Proof.
intros B HInv_1 HInv_2 HGuard B'.
  unfold B'. clear B'.
  unfold action.
  unfold Inv_2.
  simpl.
  right.
  unfold Guard in HGuard.
  destruct HGuard as [HG1 HG2].
  exact HG1.
Qed.


(**)
(* On s'est stopper ici.*)
(**)



Theorem PO_Safety:
  forall (B:State),
    (* Invariants *)
    Inv_1 B
    -> Inv_2 B
    (* Guard *)
    -> Guard B
    (* ==> post *)
    -> let B' := action B
       in Inv_1 B' /\  Inv_2 B'.
Proof.
  intros B HInv_1 HInv_2 HGuard B'.
  split.
  - (* cas : Invariant 1 *) 
    apply PO_Safety_Inv_1 ; assumption.
  - (* cas : Invariant 2 *)
    apply PO_Safety_Inv_2 ; assumption.
Qed.




(*  Convergence *)
Definition variant (B:State) : nat :=
  Context.max_nb_cars - 
  ( B.(nb_cars_to_island) + B.(nb_cars_on_island) ).

Lemma minus_S:
  forall n m : nat,
    m < n -> n - S m < n - m.
Proof.
  induction n as [|n'].
  - (* cas n=0 *)
    intros m Hcontra.
    inversion Hcontra.
  - (* cas n=S n' *)
    intros m H.
    simpl.
    destruct m as [|m'].
    + (* cas m=0 *)
      SearchRewrite ( _ - 0 ).
      (* minus_n_O: forall n : nat, n = n - 0 *)
      rewrite <- minus_n_O.
      SearchPattern ( ?X < S ?X ).
      (* lt_n_Sn: forall n : nat, n < S n *)
      apply lt_n_Sn.
    + (* cas m=S m' *)
      apply IHn'.
      SearchPattern ( S ?X < S ?Y -> ?X < ?Y ).
      (* lt_S_n: forall n m : nat, S n < S m -> n < m *)
      apply lt_S_n in H.
      exact H.
Qed.

Theorem PO_Convergence:
  (* Etat courant *)
  forall B : State,
    (* Invariants *)
    Inv_1 B
    -> Inv_2 B
    (* Guard *)
    -> Guard B
    (* Etat successeur *)
    -> let B' := action B
       in 
       (* Décroissance *)
       variant B' < variant B.
Proof.
intros B HInv_1 HInv_2 HGuard B'.
unfold B'. clear B'.
unfold variant.
unfold action.
simpl.
apply minus_S.
unfold Guard in HGuard.
destruct HGuard as [HGuard1 HGuard2].
exact HGuard2.
Qed.

End CarEnterFromMainland.






(** Evénement : CarLeaveToIsland *)
Module CarLeaveToIsland.

Definition Guard (B:State) : Prop :=
  B.(nb_cars_to_island) > 0.

Definition action (B:State) : State :=
  mkState (pred B.(nb_cars_to_island))
          B.(nb_cars_to_mainland)
          (S B.(nb_cars_on_island)).

Lemma PO_Safety_Inv_1:
  forall (B:State),
    (* Invariants *)
    Inv_1 B
    -> Inv_2 B
    (* Guard *)
    -> Guard B
    (* ==> post *)
    -> let B' := action B
       in Inv_1 B'.
Proof.
  intros B HInv_1 HInv_2 HGuard B'.
  unfold B'. clear B'.
  unfold Inv_1, action in *.
  unfold total_nb_cars in *.
  simpl.
  unfold Guard in HGuard.
  destruct (nb_cars_to_island B).
  - (* cas 0 *)
    inversion HGuard.
  - (* cas S n *)
    clear HGuard.
    simpl.
    (* auto with arith.  (* ou en mode manuel *) *)
    SearchRewrite (S _ + _).
    (* plus_Snm_nSm: forall n m : nat, S n + m = n + S m *)
    rewrite plus_Snm_nSm in HInv_1.
    rewrite <- plus_assoc in HInv_1.
    rewrite plus_Snm_nSm in HInv_1.
    rewrite plus_assoc in HInv_1.
    exact HInv_1.
Qed.

Lemma PO_Safety_Inv_2:
  forall (B:State),
    (* Invariants *)
    Inv_1 B
    -> Inv_2 B
    (* Guard *)
    -> Guard B
    (* ==> post *)
    -> let B' := action B
       in Inv_2 B'.
Proof.
  intros B HInv_1 HInv_2 HGuard B'.
  unfold B'. clear B'.
  unfold Inv_2, action in *.
  simpl.
  destruct HInv_2 as [HInv_2 | HInv_2].
  - (* cas to_island=0 *)
    unfold Guard in HGuard. 
    rewrite HInv_2 in HGuard.
    inversion HGuard.
  - (* cas to_mainland=0 *)
    right.
    exact HInv_2.
Qed.

Lemma PO_Safety:
  forall (B:State),
    (* Invariants *)
    Inv_1 B
    -> Inv_2 B
    (* Guard *)
    -> Guard B
    (* ==> post *)
    -> let B' := action B
       in Inv_1 B' /\ Inv_2 B'.
Proof.
  intros B HInv_1 HInv_2 Guard B'.
  split.
  - (* cas Inv_1 *)
    apply PO_Safety_Inv_1 ; assumption.
  - (* cas Inv_2 *)
    apply PO_Safety_Inv_2 ; assumption.
Qed.

End CarLeaveToIsland.






(** Evénement : CarEnterFromIsland *)
Module CarEnterFromIsland.

Definition Guard (B:State) : Prop :=
  B.(nb_cars_on_island) > 0
  /\ B.(nb_cars_to_island) = 0.

Definition action (B:State) : State :=
  mkState B.(nb_cars_to_island)
          (S B.(nb_cars_to_mainland))
          (pred B.(nb_cars_on_island)).

Lemma PO_Safety_Inv_1:
  forall (B:State),
    (* Invariants *)
    Inv_1 B
    -> Inv_2 B
    (* Guard *)
    -> Guard B
    (* ==> post *)
    -> let B' := action B
       in Inv_1 B'.
Proof.
  intros B HInv_1 HInv_2 HGuard B'.
  unfold B'. clear B'.
  unfold Inv_1, action in *.
  unfold total_nb_cars in *.
  simpl.
  unfold Guard in HGuard.
  destruct HGuard as [HGuard1 HGuard2].
  rewrite HGuard2 in *. clear HGuard2.
  SearchRewrite (0 + _).
  (* plus_O_n: forall n : nat, 0 + n = n *)
  rewrite plus_O_n in *.
  destruct (nb_cars_on_island) as [|n].
  - (* cas 0 *)
    inversion HGuard1.
  - (* cas S n *)
    simpl.
    SearchRewrite (S (_ + _)).
    (* plus_n_Sm: forall n m : nat, S (n + m) = n + S m *)
    rewrite plus_n_Sm.
    exact HInv_1.
Qed.

Lemma PO_Safety_Inv_2:
  forall (B:State),
    (* Invariants *)
    Inv_1 B
    -> Inv_2 B
    (* Guard *)
    -> Guard B
    (* ==> post *)
    -> let B' := action B
       in Inv_2 B'.
Proof.
  intros B HInv_1 HInv_2 HGuard B'.
  unfold B'. clear B'.
  unfold Inv_2, action in *.
  simpl.
  unfold Guard in HGuard.
  destruct HGuard as [HGuard1 HGuard2].
  left.
  exact HGuard2.
Qed.

Lemma PO_Safety:
  forall (B:State),
    (* Invariants *)
    Inv_1 B
    -> Inv_2 B
    (* Guard *)
    -> Guard B
    (* ==> post *)
    -> let B' := action B
       in Inv_1 B' /\ Inv_2 B'.
Proof.
  intros B HInv_1 HInv_2 Guard B'.
  split.
  - (* cas Inv_1 *)
    apply PO_Safety_Inv_1 ; assumption.
  - (* cas Inv_2 *)
    apply PO_Safety_Inv_2 ; assumption.
Qed.

End CarEnterFromIsland.







(** Evénement : CarLeaveToMainland *)
Module CarLeaveToMainland.

Definition Guard (B:State) : Prop :=
  B.(nb_cars_to_mainland) > 0.

Definition action (B:State) : State :=
  mkState B.(nb_cars_to_island)
          (pred B.(nb_cars_to_mainland))
          B.(nb_cars_on_island).

Lemma PO_Safety_Inv_1:
  forall (B:State),
    (* Invariants *)
    Inv_1 B
    -> Inv_2 B
    (* Guard *)
    -> Guard B
    (* ==> post *)
    -> let B' := action B
       in Inv_1 B'.
Proof.
  intros B HInv_1 HInv_2 HGuard B'.
  unfold B'. clear B'.
  unfold Inv_1, action in *.
  unfold total_nb_cars in *.
  simpl.
  unfold Guard in HGuard.
  unfold Inv_2 in HInv_2.
  destruct HInv_2 as [HInv_2 | HInv_2].
  - (* cas to_island = 0 *)
    rewrite HInv_2 in *.
    rewrite plus_O_n in *.
    assert (H1: pred (nb_cars_to_mainland B) + nb_cars_on_island B 
                <= nb_cars_to_mainland B + nb_cars_on_island B).
    { SearchPattern ( ?X <= _ -> ?X + _ <= _ ).
      (* plus_le_compat_r: forall n m p : nat, n <= m -> n + p <= m + p *)
      apply plus_le_compat_r.
      apply le_pred_n.
    }
    Check le_trans.
    (* le_trans
     : forall n m p : nat, n <= m -> m <= p -> n <= p
     *)
    apply le_trans with (m:=nb_cars_to_mainland B + nb_cars_on_island B).
    exact H1.
    exact HInv_1.
  - (* cas to_mainland = 0 *)
    rewrite HInv_2 in HGuard.
    inversion HGuard.
Qed.

Lemma PO_Safety_Inv_2:
  forall (B:State),
    (* Invariants *)
    Inv_1 B
    -> Inv_2 B
    (* Guard *)
    -> Guard B
    (* ==> post *)
    -> let B' := action B
       in Inv_2 B'.
Proof.
  intros B HInv_1 HInv_2 HGuard B'.
  unfold B'. clear B'.
  unfold Inv_2, action in *.
  simpl.
  destruct HInv_2 as [HInv_2 | HInv_2].
  - (* cas to_island = 0 *)
    left.
    exact HInv_2.
  - (* cas to_mainland = 0 *)
    unfold Guard in HGuard.
    rewrite HInv_2 in HGuard.
    inversion HGuard.
Qed.

Lemma PO_Safety:
  forall (B:State),
    (* Invariants *)
    Inv_1 B
    -> Inv_2 B
    (* Guard *)
    -> Guard B
    (* ==> post *)
    -> let B' := action B
       in Inv_1 B' /\ Inv_2 B'.
Proof.
  intros B HInv_1 HInv_2 Guard B'.
  split.
  - (* cas Inv_1 *)
    apply PO_Safety_Inv_1 ; assumption.
  - (* cas Inv_2 *)
    apply PO_Safety_Inv_2 ; assumption.
Qed.

End CarLeaveToMainland.

Theorem PO_Deadlock_Freedom:
  forall (B:State),
    (* Invariants *)
    Inv_1 B
    -> Inv_2 B
    (* Guard disjunction *)
    -> CarEnterFromMainland.Guard B
       \/ CarLeaveToIsland.Guard B
       \/ CarEnterFromIsland.Guard B
       \/ CarLeaveToMainland.Guard B.
Proof.
  intros B HInv_1 HInv_2.
  unfold Inv_1 in HInv_1.
  unfold Inv_2 in HInv_2.
  unfold CarEnterFromMainland.Guard.
  unfold CarLeaveToIsland.Guard.
  unfold CarEnterFromIsland.Guard.
  unfold CarLeaveToMainland.Guard.
  unfold total_nb_cars in HInv_1.
  destruct HInv_2 as [HInv_2 | HInv_2].
  - (* cas to_island = 0 *)
    destruct (nb_cars_on_island) as [|n].
    + (* cas 0 *)
      destruct (nb_cars_to_mainland) as [|m].
      * (* cas 0 *)
        left.
        split.
        reflexivity.
        repeat rewrite <- plus_n_O in *.
        rewrite HInv_2.
        assert (HAxiom: Context.max_nb_cars > 0).
        { apply Context.max_nb_cars_not_zero. }
        unfold gt in HAxiom.
        exact HAxiom.
      * (* cas S m *)
        right.
        right.
        right.
        SearchPattern ( S _ > 0 ).
        (* gt_Sn_O: forall n : nat, S n > 0 *)
        apply gt_Sn_O.
    + (* cas S n *)
      rewrite HInv_2 in *.
      right.
      right.
      left.
      split.
      * apply gt_Sn_O.
      * reflexivity.
  - (* cas to_mainland = 0 *)
    rewrite HInv_2 in *.
    SearchRewrite (0 + _).
    rewrite <- plus_assoc in HInv_1.
    rewrite plus_O_n in HInv_1.
    destruct (nb_cars_to_island B) as [|n].
    + (* cas 0 *)
      rewrite plus_O_n in *.
      destruct (nb_cars_on_island B) as [|m].
      * (* cas 0 *)
        left.
        split.
        reflexivity.
        assert (HAxiom: Context.max_nb_cars > 0).
        { apply Context.max_nb_cars_not_zero. }
        unfold gt in HAxiom.
        exact HAxiom.
      * (* cas S m *)
        right.
        right.
        left.
        split.
        apply gt_Sn_O.
        reflexivity.
    + (* cas S n *)    
      destruct (nb_cars_on_island B) as [|m].
      * (* cas 0 *)
        right.
        left.
        apply gt_Sn_O.
      * (* cas S m *)
        right.
        left.
        apply gt_Sn_O.
Qed.        
      
End Bridge.


