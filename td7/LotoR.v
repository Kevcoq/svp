
Require Import List.
Require Import Arith.

(* Utilitaires *)

Lemma neq_beq_false:
  forall n m : nat,
    n <> m -> beq_nat n m = false.
Proof.
  intros n m Hneq.
  case_eq (beq_nat n m).
  - (* cas true *)
    intro Htrue.
    SearchPattern (beq_nat _ _ =  true -> _ ).
    (* beq_nat_true: forall x y : nat, beq_nat x y = true -> x = y *)
    apply beq_nat_true in Htrue.
    contradiction.
  - (* cas false *)
    intro Hfalse.
    reflexivity.
Qed.

Fixpoint frequence (n:nat) (l:list nat) : nat :=
  match l with
    | nil => 0
    | m::l' => if (beq_nat n m) then
                 S (frequence n l')
               else frequence n l'
end.




(* Machine abstraite *)
Module AbstractLoto.

Module Context.

(* vide *)

End Context.


Record State : Set := mkState {
  bouboules : list nat
}.

Definition Inv_1 (B : State) : Prop :=
  forall n : nat,
    frequence n B.(bouboules) <= 1.

Module Init.

Definition Guard (bs:list nat) : Prop :=
  forall n : nat, frequence n bs <= 1.

Definition action (bs:list nat) : State :=
  mkState bs.

Theorem PO_Safety:
  forall bs : list nat,
    (* Guard *)
    Guard bs
    (* ==> post *)
    -> let B := action bs
       in Inv_1 B.
Proof.
  intros bs HGuard B.
  unfold B. clear B.
  unfold Inv_1 , action.
  unfold Guard in HGuard.
  simpl.
  exact HGuard.
Qed.  


End Init.

Module Pick.

Definition Guard (B:State) : Prop :=
  0 < length B.(bouboules).

Definition action_Prop_1 (B B': State) : Prop :=
  forall n : nat, frequence n B'.(bouboules) <= frequence n B.(bouboules).

Definition action_Prop_2 (B B': State) : Prop :=
  length B.(bouboules) = S (length B'.(bouboules)).

Definition action_witness (B : State) : State :=
  match B.(bouboules) with
    | nil => B
    | n::bs => mkState bs
  end.


Lemma PO_Feasibility_1:
  forall B : State,
    (* Invariants *)
    Inv_1 B
    (* Garde *)
    -> Guard B
    (* ==> post *)
    -> let B' := action_witness B
       in
       action_Prop_1 B B'.
Proof.
  intros B HInv_1 HGuard B'.
  unfold B'. clear B'.
  unfold action_Prop_1 , action_witness.
  unfold Inv_1 in HInv_1.
  unfold Guard in HGuard.
  intro n.
  destruct (bouboules B) as [|m bs].
  - (* cas bs=nil *)
    simpl in HGuard.
    inversion HGuard. (* contradiction *)
  - (* cas bs= m::bs *)
    simpl in *.
    clear HGuard. (* inutile *)
    destruct (eq_nat_dec n m) as [Heq | Hneq].
    + (* cas n = m *)
      rewrite Heq.
      SearchRewrite (beq_nat _ _).
      (* beq_nat_refl: forall n : nat, true = beq_nat n n *)
      rewrite <- beq_nat_refl.
      SearchPattern ( ?X <= S ?X ).
      (* le_n_Sn: forall n : nat, n <= S n *)
      apply le_n_Sn.
    + (* cas n <> m *)
      SearchRewrite (beq_nat _ _).
      (* neq_beq_false: forall n m : nat, n <> m -> beq_nat n m = false *)
      rewrite neq_beq_false.
      apply le_refl.
      exact Hneq.
Qed.

Lemma PO_Feasibility_2:
  forall B : State,
    (* Invariants *)
    Inv_1 B
    (* Garde *)
    -> Guard B
    (* ==> post *)
    -> let B' := action_witness B
       in
       action_Prop_2 B B'.
Proof.
  intros B HInv_1 HGuard B'.
  unfold B'. clear B'.
  unfold action_Prop_2 , action_witness.
  unfold Inv_1 in HInv_1.
  unfold Guard in HGuard.
  destruct (bouboules B) as [|m bs'].
  - (* cas nil *)
    simpl in HGuard.
    inversion HGuard. (* contradiction *)
  - (* cas m::bs' *)
    simpl.
    reflexivity.
Qed.

Theorem PO_Feasibility:
  forall B:State,
    (* Invariants *)
    Inv_1 B
    (* Guard *)
    -> Guard B
    (* Existence *)
    -> exists B' : State,
         action_Prop_1 B B'
         /\ action_Prop_2 B B'.
Proof.
  intros B HInv_1 HGuard.
  exists (action_witness B).
  split.
  - apply PO_Feasibility_1 ; assumption.
  - apply PO_Feasibility_2 ; assumption.
Qed.

Theorem PO_Safety:
  forall B:State,
    (* Invariants *)
    Inv_1 B
    (* Guard *)
    -> Guard B
    (* action *)
    -> forall B' : State,
         action_Prop_1 B B'
         -> action_Prop_2 B B'
         (* ==> post *)
         -> Inv_1 B'.
Proof.
  intros B HInv_1 HGuard [B' Hact1 Hact2].
  unfold Inv_1 in *.
  intro n.
  unfold Guard in HGuard.
  unfold action_Prop_1 in Hact1.
  unfold action_Prop_2 in Hact2.
  apply le_trans with (m:=frequence n (bouboules B)).
  apply Hact1.
  apply HInv_1.
Qed.

End Pick.


(* =============================================================== *)

(** * Exercice *)
(**   ** Question 1
Définir un evénement non-déterministe [Poke] qui
ajoute une boule dans l'état courant, sans paramètre.*)
Module Poke.

(* pas de guard : True *)

Definition action_Prop_1 (B B': State) : Prop :=
 forall n : nat, frequence n B'.(bouboules) <= 1.

Definition action_Prop_2 (B B': State) : Prop :=
  (length B'.(bouboules)) = S(length B.(bouboules)).


Fixpoint max (l: list nat) : nat :=
  match l with
    | nil => 0
    | n::l' => let m := max l'
               in if leb m n then n else m
  end.
Eval compute in max (2::3::1::nil).
(**  ** Question 2
Utiliser comme témoin la valeur maximale des boules
présentes plus 1.*)

Definition action_witness (B : State) : State :=
  mkState (cons (S (max B.(bouboules))) B.(bouboules)).


(**  ** Question 3
Montrer les obligations de preuves [PO_Feasibility] et [PO_Safety].*)


Lemma un_inf : 
  forall n : nat, 1 <= S n.
Proof.                          
  intro n0.
  auto with arith.
Qed.

Lemma beq_nat_sup_0 :
  forall n : nat, forall m : nat, (if beq_nat n m then 1 else 0) >= 0.
Proof.
  intros n m.
  case_eq (beq_nat n 1).
  - (* true *)
    intro ht.
    auto with arith.
  - (* false *)
    intro hf.
    auto with arith.
Qed.


Lemma freq_max_0:
  forall list : list nat, forall n : nat, max list < n -> (frequence n list) = 0.
  Proof.
    intro list.
    induction list.
    - (* cas nil *)
      simpl.
      reflexivity.
    - (* cas autre *)
      simpl.
      destruct a.
      + (* 0 *)
        destruct (max list).
        * (* 0 *)
          intros n hTruc.
          simpl in *.
          assert(beq_nat n 0 = false).
          {
            destruct n.
            - inversion hTruc.
            - auto with arith.
          }
          rewrite H.
          apply IHlist.
          exact hTruc.
        *
          intro n0.
          assert(Hleb : leb (S n) 0 = false).
          {
            auto with arith.
          }
          rewrite Hleb.
          intro truc.
          assert(0 <> n0).
          {
            SearchPattern(_ <> _).
            apply lt_0_neq.
            SearchPattern ( leb _ _ = false -> _).
            apply leb_complete_conv in Hleb.
            apply lt_trans with (m:=S n) ; assumption.
          }
          SearchPattern(?X <> ?y -> ?y <> ?X).
          apply not_eq_sym in H.
          SearchRewrite(beq_nat _ _).
          rewrite neq_beq_false.
          apply IHlist.
          exact truc.
          exact H.
      + intros n truc.
        destruct (leb (max list) (S a)) in truc.
        * assert(beq_nat n (S a) = false).
          {
            SearchPattern(beq_nat _ _ = false).
            apply neq_beq_false.
            SearchPattern(?X <> ?y -> ?y <> ?X).
            apply not_eq_sym.
            unfold not.
            intro heq.
            rewrite heq in truc.
            assert(n = n). 
            { 
              reflexivity. 
            }
            inversion truc.
            SearchPattern ( ?X <> S ?X ).
            assert (Hcontra: n <> S n). 
            { apply n_Sn. }
            apply not_eq_sym in Hcontra.
            contradiction.
(*  ------------------------------------- *)            
            SearchPattern ( _ < _ -> _ <> _ ).
            contradiction.
            inversion truc.
            inversion truc.
            trivial.
            
            rewrite H0.
            
            auto with arith.
            trivial.
            simpl.
            auto with arith.
          }
          


          induction  n.
          discriminate.
          

          destruct IHlist.
          exact IHlist.
            unfold beq_nat.
            
            simpl.





          exact IHlist.
        * (* n *)
          assert(Hleb : leb (S n) 0 = false).
          {
            auto with arith.
          }
          rewrite Hleb.
          exact IHlist.
      + (* n *)
        case_eq (leb (max list) (S a)).
        * (* 0 *)
          assert(Hbeq : beq_nat (S a) a = false).
          {
            simpl.
            induction a.
            - reflexivity.
            - simpl.
              exact IHa.
          }
          rewrite Hbeq.
          intro lebT.
          SearchPattern(leb _ _ = true -> _).
          (* leb_complete: forall m n : nat, leb m n = true -> m <= n *)
          apply leb_complete in lebT.
          SearchPattern(_ <= _ -> _ \/ _).
          (* le_lt_or_eq: forall n m : nat, n <= m -> n < m \/ n = m *)
          apply le_lt_or_eq in lebT.
          destruct lebT.
          
          inversion lebT.
          exact IHlist.
          
          unfold frequence.
          
          simpl.

        simpl.

Lemma PO_Feasibility_1:
  forall B : State,
    (* Invariants *)
    Inv_1 B
    (* ==> post *)
    -> let B' := action_witness B
       in
       action_Prop_1 B B'.
Proof.
  intros B HInv_1 B'.
  unfold B'. clear B'.
  unfold action_Prop_1 , action_witness.
  unfold Inv_1 in HInv_1.
  intro n.
  simpl.
  case_eq (beq_nat n (S (max (bouboules B)))).
  - (* true *)
    intro btrue.
    SearchPattern (beq_nat _ _ = true -> _).
    (*assert (Heq: n = S (max (bouboules B))). *)
    apply beq_nat_true in btrue.
    rewrite btrue.
    
    
    intro bbn.
    simpl.
    apply beq_nat_sup_0.
  - (* b::bs *)
    intros n0 l rec.
    simpl.
    case_eq  (leb (max l) n0). 
    + (* true *)
      intro ht.
      destruct beq_nat.
      * unfold ge.
        SearchPattern(?X <= S ?X).
        (* le_n_Sn: forall n : nat, n <= S n *)
        apply le_n_Sn.
      * unfold ge.
        trivial.
    + (* false *)
      intro hf.
      destruct beq_nat.
      * unfold ge.
        SearchPattern(?X <= S ?X).
        (* le_n_Sn: forall n : nat, n <= S n *)
        apply le_n_Sn.
      * unfold ge.
        trivial.
Qed.

Lemma PO_Feasibility_2:
  forall B : State,
    (* Invariants *)
    Inv_1 B
    (* ==> post *)
    -> let B' := action_witness B
       in
       action_Prop_2 B B'.
Proof.
  intros B HInv_1 B'.
  unfold B'. clear B'.
  unfold action_Prop_2 , action_witness.
  unfold Inv_1 in HInv_1.
  simpl.
  reflexivity.
Qed.

Theorem PO_Feasibility:
  forall B:State,
    (* Invariants *)
    Inv_1 B
    (* Existence *)
    -> exists B' : State,
         action_Prop_1 B B'
         /\ action_Prop_2 B B'.
Proof.
  intros B HInv_1.
  exists (action_witness B).
  split.
  - apply PO_Feasibility_1 ; assumption.
  - apply PO_Feasibility_2 ; assumption.
Qed.



Theorem PO_Safety:
  forall B:State,
    (* Invariants *)
    Inv_1 B
    (* action *)
    -> forall B' : State,
         action_Prop_1 B B'
         -> action_Prop_2 B B'
         (* ==> post *)
         -> Inv_1 B'.
Proof.
  intros B HInv_1. 
  intros B' Hact1 Hact2.
  unfold Inv_1 in *.
  intro n.
  unfold action_Prop_1 in Hact1.
  unfold action_Prop_2 in Hact2.
  simpl in *.
  




  simpl.
  rewrite Hact1.
  apply le_trans with (m:=frequence n (bouboules B)).
  - unfold ge in Hact1.
    rewrite Hact1.
End Poke.


(* =============================================================== *)


End AbstractLoto.


Module ConcreteLoto.

Module Context.

(* vide *)

End Context.

Record State : Set := mkState {
  bouboules : list nat
}.

Definition Glue_1 (B : State) (AB : AbstractLoto.State) : Prop := 
    B.(bouboules) = AbstractLoto.bouboules AB.

Module Init.

Definition Guard (bs:list nat) : Prop :=
  forall n : nat, frequence n bs <= 1.

Definition action (bs:list nat) : State :=
  mkState bs.

Theorem PO_Strengthening:
  (* Paramètres *)
  forall bs : list nat,
    (* Garde concrète *)
    Guard bs
    -> (* ====> *)
    (* Garde abstraite *)
    AbstractLoto.Init.Guard bs.
Proof.
  intros bs HGuard.
  unfold AbstractLoto.Init.Guard.
  unfold Guard in HGuard.
  exact HGuard.
Qed.

(*  Pas de PO_Safety : pas d'invariant concrete
    (autre que la glue) *)


Theorem PO_Simulation:
  forall bs : list nat,
    (* Garde concrete *)
    Guard bs
    -> let B := action bs
       in
       let AB := AbstractLoto.Init.action bs
       in
       (* Invariant concret *)
       Glue_1 B AB.
Proof.
  intros bs HGuard B AB.
  unfold B. clear B.
  unfold AB. clear AB.
  unfold Glue_1, action, AbstractLoto.Init.action.
  simpl.
  reflexivity.
Qed.

End Init.

Module Pick.

Definition Guard (B:State) : Prop :=
  0 < length B.(bouboules).


Fixpoint min (l: list nat) : nat :=
  match l with
    | nil => 0
    | n::l' => let m := min l'
               in if leb m n then n else m
  end.

Fixpoint remove_elem (n:nat) (l : list nat) : list nat :=
  match l with
    | nil => l
    | m::l' => if beq_nat n m then l' 
               else m::(remove_elem n l')
  end.

Definition action (B:State) : State :=
  mkState 
    (remove_elem (min B.(bouboules)) B.(bouboules)).

Theorem PO_Strengthening:
  forall B : State, forall AB : AbstractLoto.State,
    (* Invariants abstraits *)
    AbstractLoto.Inv_1 AB
    (* Invariants de glue *)
    -> Glue_1 B AB
    (* Invariants concrets *)
    (* aucun *)
    (* Garde concrète *)
    -> Guard B
    (* ==> post *)
    -> AbstractLoto.Pick.Guard AB.
Proof.
  intros B AB HAbsInv_1 HGlue_1 HGuard.
  unfold AbstractLoto.Pick.Guard, action.
  unfold Glue_1 in HGlue_1.
  unfold Guard in HGuard.
  rewrite <-HGlue_1.
  exact HGuard.
Qed.

(* pas de PO_Safety : pas de nouvel invariant *)

Lemma lt_diff:
  forall n m : nat, n < m -> n <> m.
Proof.
  induction n as [|n'].
  - (* cas n=0 *)
    intros m Hlt.
    destruct m as [|m'].  (* auto with arith OK *)
    + (* cas m=0 *) 
      inversion Hlt. (* contradiction *)
    + (* cas m=S m' *)
      SearchPattern ( 0 <> _ ).
      (* O_S: forall n : nat, 0 <> S n *)
      apply O_S.
  - (* cas n=S n' *)
    intros m Hlt.
    destruct m as [|m'].
    + (* cas m=0 *) (* auto with arith OK *)
      SearchPattern ( ?X <> ?Y  -> ?Y <> ?X ).
      (* not_eq_sym: forall (A : Type) (x y : A), x <> y -> y <> x *)
      apply not_eq_sym.
      apply O_S.
    + (* cas m=S m' *)
      SearchPattern ( S _ <> S _ ).
      (* not_eq_S: forall n m : nat, n <> m -> S n <> S m *)
      apply not_eq_S.
      apply IHn'.
      SearchPattern (S _ < S _ -> _ ).
      (* lt_S_n: forall n m : nat, S n < S m -> n < m *)
      apply lt_S_n.
      exact Hlt.
Qed.  


Lemma frequence_min_remove:
  forall n : nat, forall l : list nat,
    frequence n (remove_elem (min l) l) <=
    frequence n l.
Proof.
  induction l as [| e l'].
  - (* cas l=nil *)
    simpl.
    apply le_refl.
  - (* cas l=e::l' *)
    simpl.
    case_eq (leb (min l') e).
    + (* cas true *)
      intro Hleb.
      SearchRewrite (beq_nat ?X ?X).
      (* beq_nat_refl: forall n : nat, true = beq_nat n n *)
      rewrite <- beq_nat_refl.
      destruct (beq_nat n e).
      * (* cas true *)
        SearchPattern (?X <= S ?X).
        (* le_n_Sn: forall n : nat, n <= S n *)
        apply le_n_Sn.
      * (* cas false *) 
        apply le_refl.
    + (* cas false *)
      intro Hnleb.
      assert (Hnle: e < min l' ).
      { SearchPattern ( leb _ _ = false -> _ ).
        (* leb_complete_conv: forall m n : nat, leb n m = false -> m < n *)
        apply leb_complete_conv.
        exact Hnleb.
      }
      assert (Hneq: e <> min l').
      { apply lt_diff.
        exact Hnle.
      }    
      assert (Hnbeq: beq_nat (min l') e = false).
      { SearchPattern (beq_nat _ _ = false).
        (* neq_beq_false: forall n m : nat, n <> m -> beq_nat n m = false *)
        apply neq_beq_false.
        apply not_eq_sym.
        exact Hneq.
      }
      rewrite Hnbeq.
      simpl.
      destruct (beq_nat n e).
      * (* cas true *)
        SearchPattern ( ?X <= ?Y -> S ?X <= S ?Y ).
        (* le_n_S: forall n m : nat, n <= m -> S n <= S m *)
        apply le_n_S.
        exact IHl'.
      * (* cas false *)
        exact IHl'.
Qed.

Lemma length_remove_min:
  forall l : list nat,
    0 < length l -> length l = S (length (remove_elem (min l) l)).
Proof.
  induction l as [|e l'].
  - (* cas l=nil *)
    intro Hcontra.
    simpl in *.
    inversion Hcontra.
  - (* cas l=e::l' *)
    {
      simpl.     
      intro Hdummy. clear Hdummy.
      apply eq_S.
      destruct l' as [|e' l''].
      - (* cas l'=nil *)
        simpl in IHl'.
        simpl.
        rewrite <- beq_nat_refl.
        simpl.
        reflexivity.
      - (* cas l'=e'::l'' *)
        simpl in *.
        case_eq (leb (min l'') e').
        + (* cas true *)
          intro Hleb.
          rewrite Hleb in IHl'.
          case_eq (leb e' e).
          * (* cas true *)
            intro Hleb'.
            rewrite <- beq_nat_refl.
            simpl.
            reflexivity.
          * (* cas false *)
            intro Hnleb.
            rewrite <- beq_nat_refl.
            destruct (beq_nat e' e) ; reflexivity.
        + (* cas false *)
          intro Hnleb.
          rewrite Hnleb in IHl'.
          case_eq (leb (min l'') e).
          * (* cas true *)
            intro Hleb.
            rewrite <- beq_nat_refl.
            simpl.
            reflexivity.
          * (* cas false *)
            intro Hnleb'.
            { destruct (beq_nat (min l'') e).
              - (* cas true *)
                simpl.
                reflexivity.
              - (* cas false *)
                simpl.
                apply IHl'.
                SearchPattern ( 0 < S _ ).
                (* lt_0_Sn: forall n : nat, 0 < S n *)
                apply lt_0_Sn.
            }                
    }
Qed.

Theorem PO_Simulation:
  forall (B:State), forall (AB:AbstractLoto.State),
    (* Invariants abstraits *)
    AbstractLoto.Inv_1 AB
    (* Invariants de glue *)
    -> Glue_1 B AB
    (* Invariants concrets *)
    (* aucun *)
    (* Garde concrète *)
    -> Guard B
    (* ==> post *)
    -> let B' := action B
       in exists AB' : AbstractLoto.State,
            AbstractLoto.Pick.action_Prop_1 AB AB'
            /\ AbstractLoto.Pick.action_Prop_2 AB AB'
            (* Invariants de glue *)
            /\ Glue_1 B' AB'.
Proof.
  intros B AB HAbsInv_1 HGlue_1 HGuard B'.
  unfold B' in *. clear B'.
  exists (AbstractLoto.mkState 
            (remove_elem (min (AbstractLoto.bouboules AB)) 
                         (AbstractLoto.bouboules AB))).
  unfold AbstractLoto.Pick.action_Prop_1.
  unfold AbstractLoto.Pick.action_Prop_2.
  simpl.
  unfold Glue_1 in *.
  rewrite <- HGlue_1 in *.
  simpl.
  split.
  - (* cas left *)
    intro n.
    apply frequence_min_remove.
  - { split.
      - (* cas middle *)
        apply length_remove_min.
        unfold Guard in HGuard.
        exact HGuard.
      - (* cas right *)
        reflexivity.
    }        
Qed.

End Pick.

(* =============================================================== *)

(** * Exercice *)

(** 

  ** Question 1

Proposer un raffinement de l'opération abstraite [Poke] telle que la
boule ajoutée est le premier "trou" rencontré  (boule dont le numéro
non-présent est minimal).

  ** Question 2

Montrer les obligations de preuve [PO_Strengthening] et [PO_Simulation].

*)

(* =============================================================== *)

End ConcreteLoto.
